# Algorithms

Implementations to basic abstract data types (ADTs) and algorithms.

Also, solutions to some leetcode problems.

## Requirements

- Python 3
- pytest

## Library

The directory `lib` contains implementations to reusable algorithms
and data structures.

## Leetcode

The directory `leetcode` contains my solutions to some algorithms problems
on leetcode.

The solution to each problem is contained in a single Python file with a name
like `test_<problem_name>.py`, along with unit tests.

## Testing

Individual files and individual tests:

```
pytest --durations=0 -vv <problem_name>.py -k "test_name"
```

All files and all tests:

```
pytest
```

## Problems

- [x] Two Sum
- [ ] 3Sum
- [x] Longest Common Prefix
- [x] Valid Parentheses
- [x] Merge Two Sorted Lists
- [x] Remove Element
- [x] Implement strStr()
- [x] Search Insert Position
- [x] Count and Say
- [x] Pow(x, n)
- [x] Same Tree
- [x] Maximum Sub-array
- [x] Length of Last Word
- [x] Plus One
- [x] Sqrt(x)
- [x] Climbing Stairs - Interesting analysis here:
- [x] Remove Duplicates from Sorted List
- [x] Remove Duplicates from Sorted List II
- [x] Merge Sorted Array
- [x] Decode Ways
- [x] Interleaving String
- [x] Validate Binary Search Tree
- [x] Valid Anagram
- [x] Distant Barcodes
- [x] Kth Smallest Element in a Sorted Matrix
- [x] Largest Number
- [x] Largest Rectangle in Histogram
- [x] Word Break (medium)
- [ ] Word Break II (hard)
- [x] Valid Palindrome (easy)
- [x] Word Ladder (medium)
- [x] Gas Station (medium)
- [x] Reorganize String (medium)
- [x] Task Scheduler (medium)
- [ ] Median of Two Sorted Arrays (hard)
- [ ] Regular Expression Matching (medium)
- [ ] Longest Palindromic Substring (medium)
- [ ] Zigzag Conversion
- [ ] Container with Most Water
- [ ] https://en.wikipedia.org/wiki/Longest_common_substring_problem
- [ ] https://en.wikipedia.org/wiki/Heap%27s_algorithm
