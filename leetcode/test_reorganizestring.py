###############################################################################
# Filename: test_reorganizestring.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_reorganizestring.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def reorganizeString(self, S: str) -> str:
        """
        Reorganize string such that no adjacent words are equal.

        Return the reorganized string or empty string if not possible.

        Notes:
          - The string will consist of lowercase letters
          - The string will have a length in the range [1, 500]

        Solution copied from Leetcode.

        Intuition:
          Interleave the letters with different characters in between.
          For this, first sort the characters. If some character repeats
          more than (N + 1) / 2 times, then there is no solution.

        Algorithm:
          Sort the characters by increasing count of occurrence. Selected the
          even indexes then interleave the odd indexes.

        Time complexity:
          O(n log n): The cost of sorting the string of n characters.

        """
        N = len(S)
        array = []
        for count, char in sorted((S.count(x), x) for x in set(S)):
            if count > (N+1) / 2:
                return ''
            array.extend(char * count)
        ans = [None] * N
        ans[::2], ans[1::2] = array[N//2:], array[:N//2]
        return ''.join(ans)


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():

    S = 'aab'
    S = 'aaab'

    print(Solution().reorganizeString(S))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    S = 'aab'

    expect = 'aba'

    actual = Solution().reorganizeString(S)

    assert actual == expect


def test_example_2():

    S = 'aaab'

    expect = ''

    actual = Solution().reorganizeString(S)

    assert actual == expect
