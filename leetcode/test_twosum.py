###############################################################################
# Filename: test_twosum.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_twosum.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def twoSum(self, nums, target):
        """
        Find indices i and j whose values sum to target.

        Notes:
          - There is exactly one solution
          - Cannot repeat an element

        Algorithm:
          Iterate over the list. For each element j in the list
            1. If the complement (target - element) i exists in a hash table
               that contains other elements of the array,
               return the indices [i, j]
            2. Else, place element j in hash table for next iteration

        Args:
          nums (list): Integers.
          target (int): Integer.

        Returns:
          list[int]: First and second indices.

        """

        # Initialize hash table.
        hashtable = {}

        # Find indices of the two values that sum to target.
        for second_index, second_val in enumerate(nums):

            # Find index of complement in hash table.
            first_index = hashtable.get(target - second_val)

            # If complement found, return indices.
            if first_index is not None:
                return [first_index, second_index]

            # Memoize second_value for next iteration.
            hashtable[second_val] = second_index

        # No solution found.
        return None


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    nums = [2, 7, 11, 15]
    target = 9
    print(Solution().twoSum(nums, target))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_simple():

    nums = [2, 7, 11, 15]
    target = 9
    expect = [0, 1]

    actual = Solution().twoSum(nums, target)

    assert actual == expect
