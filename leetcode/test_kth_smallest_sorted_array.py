###############################################################################
# Filename: test_twosum.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_twosum.py
#
###############################################################################

import sys
import heapq
from typing import List


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def kthSmallest(self, matrix: List[List[int]], k: int) -> int:
        """
        Find kth smalles element in the matrix.

        Notes:
          - Each of the rows and columns are sorted in ascending order
          - k is always valid, 1 ≤ k ≤ n^2

        Algorithm:
          Starting at index zero for each row, select smallest from all rows
          and advance row index of the row with the smallest. Repeat k times.
          The smallest in each iteration is found by maintaining a heap of
          the current values.

        Time complexity:
          O(n + k log n)

        """
        # Initialize heap of tuples: (value, row index, row)
        # Time complexity: O(n)
        heap = []
        for row in matrix:
            heap.append((row[0], 0, row))
        heapq.heapify(heap)

        # Extract k items from the heap, every time replacing the extracted
        # item with the next item in its row.
        # Time complexity: O(k log n)
        for i in range(k):
            (value, index, row) = heapq.heappop(heap)
            index += 1
            if index < len(row):
                heapq.heappush(heap, (row[index], index, row))

        return value

        # Naive, slow version without efficient search of smallest.
        # ret = None
        # index = [0] * len(matrix)
        # for i in range(k):
        #     ret, index = self._nextSmallestElement(matrix, index)
        # return ret

    # def _nextSmallestElement(self, matrix, index):
    #     row = -1
    #     smallest = sys.maxsize
    #     for i in range(len(matrix)):
    #         # All elements in the row have already been selected.
    #         if index[i] == len(matrix):
    #             continue
    #         # Smaller element found.
    #         element = matrix[i][index[i]]
    #         if element < smallest:
    #             smallest = element
    #             row = i
    #     # Increment the index of the row with the smallest element.
    #     index[row] += 1
    #     return smallest, index


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    matrix = [
        [1,  5,  9],
        [10, 11, 13],
        [12, 13, 15]
    ]
    k = 8

    print(Solution().kthSmallest(matrix, k))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example():
    matrix = [
        [1,  5,  9],
        [10, 11, 13],
        [12, 13, 15]
    ]
    k = 8

    expect = 13

    actual = Solution().kthSmallest(matrix, k)

    assert actual == expect


def test_tricky():
    matrix = [
        [1,  5,  9, 12],
        [10, 11, 13, 30],
        [12, 13, 15, 18]
    ]
    k = 5

    expect = 11

    actual = Solution().kthSmallest(matrix, k)

    assert actual == expect
