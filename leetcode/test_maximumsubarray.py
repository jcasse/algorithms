###############################################################################
# Filename: test_maximumsubarray.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_maximumsubarray.py
#
###############################################################################

import sys
from typing import List


###############################################################################
# Solution
###############################################################################

class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        """
        Compute maximum sum of contiguous subarray.

        Algorithm:
          Dynamic programming, where we ignore the sum of the n-1 elements
          if the nth element is greater than the sum.

        """
        ret: int = nums[0]
        maxsum: int = nums[0]
        for i in range(1, len(nums)):
            tempsum = maxsum + nums[i]
            maxsum = nums[i] if nums[i] > tempsum else tempsum
            ret = ret if ret > maxsum else maxsum
        return ret


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4]
    print(Solution().maxSubArray(nums))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_empty():

    nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4]

    expect = 6

    actual = Solution().maxSubArray(nums)

    assert actual == expect
