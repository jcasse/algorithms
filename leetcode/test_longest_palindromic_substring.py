###############################################################################
# Filename: test_longest_palindromic_substring.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line:
#          pytest test_longest_palindroimic_substrig.py -v
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def longestPalindrome(self, s: str) -> str:
        """
        Find longest palindromic substring of s.

        Notes:
            - s has a maximum length of 1000

        Example:
            Input: "babad"
            Output: "bab"
            Output: true
            Note: "aba" is also a valid answer.

        Algorithm:
            Check each substring, from largest to smallest, for palindrome.
            Stop when find the first palindrome. Time complexity is n^2.

        """
        # Empty string.
        if not s:
            return s

        # Initialize to smallest palindromic sub-string.
        longest = s[0]

        # Find longer palindromic sub-string.
        for l in range(len(s)):
            for r in reversed(range(l+1, len(s))):
                print('{} {}'.format(l, r))
                if isPalindromicSubstring(s, l, r):
                    substring = s[l:r+1]
                    if len(substring) > len(longest):
                        longest = substring

        return longest


def isPalindromicSubstring(s: str, l: int, r: int) -> bool:
    """
    Check if sub-string, defined by l and r, is a palindrome.

    """
    assert l < r
    assert l >= 0 and r >= 0
    assert l < len(s) and r < len(s)

    while l < r:

        print('{} {}'.format(s[l], s[r]))
        if s[l] != s[r]:
            return False

        l += 1
        r -= 1

    return True


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():

    s = 'babad'

    print(Solution().longestPalindrome(s))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################


def test_empty_s():

    s = ''

    expect = ''

    actual = Solution().longestPalindrome(s)

    assert actual == expect


def test_example_1():

    s = 'babad'

    expect = 'bab'

    actual = Solution().longestPalindrome(s)

    assert actual == expect


def test_example_2():

    s = 'cbbd'

    expect = 'bb'

    actual = Solution().longestPalindrome(s)

    assert actual == expect
