###############################################################################
# Filename: test_deleteduplicates2.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_deleteduplicates2.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class ListNode:
    """
    Definition for singly-linked list node.
    """
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        """
        Delete all nodes that have duplicate numbers.

        Numbers that have duplicates will be removed, leaving only unique
        numbers from the original list.

        Notes:

          - The list is sorted.

        Algorithm:

          Maintain three pointers. One points to the head of the resulting
          list. Another points to the last element of the resulting list of
          unique elements. The third one points to the current element in the
          original list.
          Iterate over the original list, checking whether the current element
          is a duplicate by inspecting the element that follows it. Duplicate
          elements are dropped and the next pointer is updated to the next
          unique element or None (the end of the list).

        Pseudo code:

          while current_node not None:
            if current_node.next.val != current_node.val
              advance resulting list last pointer to next node
            else:
              drop duplicate nodes and update last element of resulting
              list to point to the next unique element or None (end of list)

        """
        if head:

            curr = head
            head = None

            dup = False
            while curr:

                # Current node has no next node with same value.
                if not curr.next or curr.val != curr.next.val:
                    if dup is False:
                        if not head:  # First unique node
                            head = curr
                            ptr = head
                        else:             # Subsequent unique node
                            ptr.next = curr
                            ptr = curr
                    else:
                        dup = False

                        # Close resulting list if last values are duplicates.
                        if head and not curr.next:
                            ptr.next = None

                # Current node has next node with same value.
                else:
                    dup = True

                curr = curr.next

        return head


###############################################################################
# Test functions
###############################################################################

def linkedListFromArray(array):
    if not array:
        return None
    head = ListNode(array[0])
    pointer = head
    for i in range(1, len(array)):
        pointer.next = ListNode(array[i])
        pointer = pointer.next
    return head


def arrayFromLinkedList(head):
    array = []
    while head:
        array.append(head.val)
        head = head.next
    return array


###############################################################################
# Driver
###############################################################################

def main():
    head = linkedListFromArray([1, 2, 3, 3, 4, 4, 5])
    print(arrayFromLinkedList(head))
    print(arrayFromLinkedList(Solution().deleteDuplicates(head)))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    head = linkedListFromArray([1, 2, 3, 3, 4, 4, 5])

    expect = [1, 2, 5]

    actual = arrayFromLinkedList(Solution().deleteDuplicates(head))

    assert actual == expect


def test_example_2():

    head = linkedListFromArray([1, 1, 1, 2, 3])

    expect = [2, 3]

    actual = arrayFromLinkedList(Solution().deleteDuplicates(head))

    assert actual == expect


def test_empty():

    head = linkedListFromArray([])

    expect = []

    actual = arrayFromLinkedList(Solution().deleteDuplicates(head))

    assert actual == expect


def test_single():

    head = linkedListFromArray([1])

    expect = [1]

    actual = arrayFromLinkedList(Solution().deleteDuplicates(head))

    assert actual == expect


def test_all_duplicates():

    head = linkedListFromArray([1, 1, 1, 1, 1])

    expect = []

    actual = arrayFromLinkedList(Solution().deleteDuplicates(head))

    assert actual == expect


def test_duplicates_last():

    head = linkedListFromArray([1, 2, 2])

    expect = [1]

    actual = arrayFromLinkedList(Solution().deleteDuplicates(head))

    assert actual == expect
