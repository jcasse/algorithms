###############################################################################
# Filename: test_gasstation.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_gasstation.py
#
###############################################################################

from typing import List


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def canCompleteCircuit(self, gas: List[int], cost: List[int]) -> int:
        """
        Find gas station where to start a complete loop.

        Return -1 if not possible to complete a loop.

        Notes:
          - If there exists a solution, it is guaranteed to be unique
          - Both input arrays are non-empty and have the same length
          - Each element in the input arrays is a non-negative integer

        Intuition:
          If a complete loop is possible, the total sum of gas minus the
          total cost must be zero or more.

        Algorithm:
          Maintain total sum of gas and cost. This will determine if there is
          a solution. Iterate over all gas stations, keeping track of the
          starting station. Whenever there is not enough gas to reach the
          current gas station from the previous, update starting gas station
          to current.

        Time complexity:
          O(n)

        """
        remaining = 0
        total = 0
        start = 0

        for i in range(len(gas)):
            newgas = gas[i] - cost[i]
            total += newgas
            if remaining >= 0:
                remaining += newgas
            else:
                start = i
                remaining = newgas

        # There is a solution.
        if total >= 0:
            return start

        # There is no possible solution.
        else:
            return -1


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():

    gas = [1, 2, 3, 4, 5]
    cost = [3, 4, 5, 1, 2]
    gas = [2, 3, 4]
    cost = [3, 4, 3]

    print(Solution().canCompleteCircuit(gas, cost))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    gas = [1, 2, 3, 4, 5]
    cost = [3, 4, 5, 1, 2]

    expect = 3

    actual = Solution().canCompleteCircuit(gas, cost)

    assert actual == expect


def test_example_2():

    gas = [2, 3, 4]
    cost = [3, 4, 3]

    expect = -1

    actual = Solution().canCompleteCircuit(gas, cost)

    assert actual == expect
