###############################################################################
# Filename: test_powxn.py
# Environment: Python2
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_powxn.py
#
###############################################################################

import pytest


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def myPow(self, x, n):
        """
        Compute x^n.

        Notes:
          - -100.0 < x < 100.0
          - n is a 32-bit signed integer, within range [-2^31, 2^31 - 1]

        :type x: float
        :type n: int
        :rtype: float
        """
        if n == 0:
            return 1
        if n < 0:
            return 1 / self.myPow(x, -n)
        if n % 2:
            return x * self.myPow(x, n - 1)
        return self.myPow(x * x, n / 2)


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    x = 2.00000
    n = 10
    print(Solution().myPow(x, n))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_simple():

    x = 2.00000
    n = 10

    expect = 1024.00000

    actual = Solution().myPow(x, n)

    assert actual == pytest.approx(expect)


def test_fractional_x():

    x = 2.10000
    n = 3

    expect = 9.26100

    actual = Solution().myPow(x, n)

    assert actual == pytest.approx(expect)


def test_negative_n():

    x = 2.00000
    n = -2

    expect = 0.25000

    actual = Solution().myPow(x, n)

    assert actual == pytest.approx(expect)


def test_large():

    x = 0.00001
    n = 2147483647

    expect = 0.00000

    actual = Solution().myPow(x, n)

    assert actual == pytest.approx(expect)
