###############################################################################
# Filename: test_wordladder.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_wordladder.py
#
###############################################################################

from collections import defaultdict
from typing import List


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def ladderLength(self,
                     beginWord: str, endWord: str, wordList: List[str]) -> int:
        """
        Compute length of shortest edit sequence from beginWord to endWord.

        A valid transformation consists of every intermediate word existing in
        the wordList, where each intermediate word changes the previous word
        by exactly one character.

        Example:
          beginWord = 'hit'
          endWord = 'cog'
          wordList = ['hot', 'dot', 'dog', 'lot', 'log', 'cog']
          sequence = 'hit' -> 'hot' -> 'dot' -> 'dog' -> 'cog'
          length = 5

        Notes:
          - One letter can be changed at a time to form a transformed word
          - Every transformed word must exist in the word list
          - beginWord is not a transformed word
          - Return 0 if there is no such transformation sequence
          - All words have the same length
          - All words contain only lowercase alphabetic characters
          - You may assume no duplicates in the word list
          - You may assume beginWord and endWord are non-empty and not the same

        Intuition:
          Each word in the problem is a node in an undirected graph, with
          edges between nodes that differ by exactly one character. The goal
          is to find the shortest path from beginWord to endWord.

        Algorithm:
          Breadth-first search can find the shortest path (sequence) from
          beginWord to endWord.

        Time complexity:
          O(L * |V| + |V| + |E|): For all words, need to check all characters.
                                  In the worst case, all nodes and edges are
                                  inspected.

        """
        if endWord not in wordList:
            return 0

        # Compute regex dict that maps a regex to list of words that match it.
        regex_dict = self._regex_dict(wordList)

        # Breadth-first search.
        queue = [(beginWord, 1)]
        visited = set(beginWord)
        while queue:

            # Get next node in breadth-first order.
            u, level = queue.pop(0)

            # End of word ladder. Return number of hops.
            if u == endWord:
                return level

            # Continue searching.
            for v in self._adjacentWords(u, regex_dict):
                if v not in visited:
                    visited.add(v)
                    queue.append((v, level + 1))

        # No word ladder found that reaches the endWord.
        return 0

    def _regex_dict(self, wordList):
        """
        Construct mapping from regex to list of words that match it.

        Time complexity:
          O(L * |v|): Length, L,  of words times number of words, |V|.

        """
        ret = defaultdict(list)
        for word in wordList:
            for i in range(len(word)):
                ret[word[:i] + '*' + word[i+1:]].append(word)
        return ret

    def _adjacentWords(self, word, intermediate_dict):
        """
        Find adjacent words to word.

        Time complexity:
          O(L): L is the length of the word.

        """
        ret = []
        for i in range(len(word)):
            intermediate_regex = word[:i] + '*' + word[i+1:]
            ret += intermediate_dict[intermediate_regex]
        return ret


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():

    beginWord = 'hit'
    endWord = 'cog'
    wordList = ['hot', 'dot', 'dog', 'lot', 'log', 'cog']
    beginWord = 'hit'
    endWord = 'cog'
    wordList = ['hot', 'dot', 'dog', 'lot', 'log']
    beginWord = 'a'
    endWord = 'c'
    wordList = ['a', 'b', 'c']

    print(beginWord)
    print(endWord)
    print(wordList)
    print(Solution().ladderLength(beginWord, endWord, wordList))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    beginWord = 'hit'
    endWord = 'cog'
    wordList = ['hot', 'dot', 'dog', 'lot', 'log', 'cog']

    expect = 5

    actual = Solution().ladderLength(beginWord, endWord, wordList)

    assert actual == expect


def test_example_2():

    beginWord = 'hit'
    endWord = 'cog'
    wordList = ['hot', 'dot', 'dog', 'lot', 'log']

    expect = 0

    actual = Solution().ladderLength(beginWord, endWord, wordList)

    assert actual == expect


def test_7_of_40():

    beginWord = 'a'
    endWord = 'c'
    wordList = ['a', 'b', 'c']

    expect = 2

    actual = Solution().ladderLength(beginWord, endWord, wordList)

    assert actual == expect


def test_19_of_40():

    beginWord = 'qa'
    endWord = 'sq'
    wordList = ['si', 'go', 'se', 'cm', 'so', 'ph', 'mt', 'db', 'mb', 'sb',
                'kr', 'ln', 'tm', 'le', 'av', 'sm', 'ar', 'ci', 'ca', 'br',
                'ti', 'ba', 'to', 'ra', 'fa', 'yo', 'ow', 'sn', 'ya', 'cr',
                'po', 'fe', 'ho', 'ma', 're', 'or', 'rn', 'au', 'ur', 'rh',
                'sr', 'tc', 'lt', 'lo', 'as', 'fr', 'nb', 'yb', 'if', 'pb',
                'ge', 'th', 'pm', 'rb', 'sh', 'co', 'ga', 'li', 'ha', 'hz',
                'no', 'bi', 'di', 'hi', 'qa', 'pi', 'os', 'uh', 'wm', 'an',
                'me', 'mo', 'na', 'la', 'st', 'er', 'sc', 'ne', 'mn', 'mi',
                'am', 'ex', 'pt', 'io', 'be', 'fm', 'ta', 'tb', 'ni', 'mr',
                'pa', 'he', 'lr', 'sq', 'ye']

    expect = 5

    actual = Solution().ladderLength(beginWord, endWord, wordList)

    assert actual == expect


def test_29_of_40():

    beginWord = 'sand'
    endWord = 'acne'
    with open('data/wordladder.csv', 'r') as f:
        wordList = list(f.read().split(','))
    print(wordList)

    expect = 11

    actual = Solution().ladderLength(beginWord, endWord, wordList)

    assert actual == expect
