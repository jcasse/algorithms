###############################################################################
# Filename: test_largestrectanglehistogram.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_largestrectanglehistogram.py
#
###############################################################################

from typing import List


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def largestRectangleArea(self, heights: List[int]) -> int:
        """
        Find the area of largest rectangle in the histogram.

        Notes:
          - The histogram is represented by a list of non-negative heights

        Algorithm:
          In a single pass, compute the areas of all possible rectangles.
          This is done by keeping track of the start and end of each height
          using a stack that stores the height and position where it first
          started. As long as the next rectangle is the same height or higher,
          we add it to the stack (with its position). Once we see a shorter
          height, we pop heights larger and compute the areas they form.

          https://www.youtube.com/watch?v=VNbkzsnllsU

        Time complexity:
          O(n) - Each height is placed in the stack at most once, and
                 each height is removed from the stack at most once.

        """
        maxArea = 0

        if not heights:
            return maxArea

        # Initialize stack (allocate static memory for speed).
        stack = [(-1, 0)] * len(heights)
        stackIndex = -1

        # Compute all areas in a single pass.
        for i in range(len(heights)):

            newHeight = heights[i]

            # Case where new height is smaller, ending the previous rectangle.
            maxArea, stackIndex = self._processRectangles(stack, stackIndex,
                                                          newHeight, i,
                                                          maxArea)

        # Process any remaining heights at the end of the list.
        maxArea, _ = self._processRectangles(stack, stackIndex,
                                             0, len(heights), maxArea)

        return maxArea

    def _processRectangles(self, stack, stackIndex,
                           newHeight, newIndex, maxArea):
        # Process rectangles if new height is smaller.
        popped = False
        while stackIndex >= 0 and newHeight < stack[stackIndex][1]:

            # Pop from stack.
            (startIndex, height) = stack[stackIndex]
            stackIndex -= 1
            popped = True

            # Update max area.
            width = (newIndex - startIndex)
            area = width * height
            maxArea = max(area, maxArea)

        # Add new height onto stack.
        # Keep start index of last popped height; it covered the new height.
        if popped:
            stackIndex += 1
            stack[stackIndex] = (startIndex, newHeight)

        # Add first height on stack or bigger height.
        elif stackIndex == -1 or newHeight > stack[stackIndex][1]:
            stackIndex += 1
            stack[stackIndex] = (newIndex, newHeight)

        # If new height is same as previous, no need to push onto stack.
        else:
            pass

        return maxArea, stackIndex

    #     # Naive O(n^2)
    #     # For each height in the list, find the largest number of consecutive
    #     # list elements that are at least that height. Compute the highest
    #     # area for each height.
    #     maxArea = 0
    #     for height in heights:
    #         # print('height {}'.format(height))
    #         maxWidth = self._maxConsecutiveBars(height, heights)
    #         # print('max width {}'.format(maxWidth))
    #         area = height * maxWidth
    #         if area > maxArea:
    #             maxArea = area
    #     return maxArea

    # def _maxConsecutiveBars(self, height, heights):
    #     maxCount = 0
    #     count = 0
    #     for x in heights:
    #         if x < height:
    #             maxCount = max(maxCount, count)
    #             count = 0
    #         else:
    #             count += 1
    #     return max(maxCount, count)


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    heights = [2, 1, 2]
    print(Solution().largestRectangleArea(heights))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example():

    heights = [2, 1, 5, 6, 2, 3]

    expect = 10

    actual = Solution().largestRectangleArea(heights)

    assert actual == expect


def test_empty():

    heights = []

    expect = 0

    actual = Solution().largestRectangleArea(heights)

    assert actual == expect


def test_other():

    heights = [2, 1, 2]

    expect = 3

    actual = Solution().largestRectangleArea(heights)

    assert actual == expect


def no_test_large():

    with open('data/histogram.csv', 'r') as f:
        heights = list(map(int, f.read().split(',')))

    expect = 10

    actual = Solution().largestRectangleArea(heights)

    assert actual == expect
