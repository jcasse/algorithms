###############################################################################
# Filename: test_wordbreak.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_wordbreak.py
#
###############################################################################

from typing import List, Union, Dict


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        """
        Determine if s can be segmented into one or more dictionary words.

        Notes:
          - The word s is non-empty
          - The dictionary contains non-empty words
          - The dictionary does not contain duplicate words
          - Words in the dictionary may be reused

        Intuition:
          Recursive algorithm with memoization (dynamic programming).
          Consider all possible matches by finding solutions to smaller
          problems, remembering those solutions to avoid repeating computation.

        Algorithm:
          Starting with the full word as the root of the recursive tree,
          find all possible matches of the beginning of the word.
          If the match covers the entire word, return True, else recurse on
          the remaining of the word.

        """
        solutions = {}
        return self._recursiveWordBreak(s, wordDict, solutions)

    def _recursiveWordBreak(self, s: str, wordDict: List[str],
                            solutions: Dict[str, bool]) -> bool:

        # If known sub-problem, do no repeat.
        solution = solutions.get(s)
        if solution is not None:
            if solution is True:
                return True
            if solution is False:
                return False

        # Solve sub-problem.
        for word in wordDict:

            # Match word with beginning of string s.
            remainder = self._matchPrefix(s, word)

            # Base case: Entire string segmented.
            if remainder == '':
                solutions[s] = True
                return True

            # Continue recursive segmentation.
            elif remainder:
                if self._recursiveWordBreak(remainder, wordDict, solutions):
                    solutions[s] = True
                    return True

            # No match.
            else:
                pass

        # No solution.
        solutions[s] = False

        return False

    def _matchPrefix(self, s: str, word: List[str]) -> Union[str, None]:
        """
        Determine if word is a prefix of s.

        Returns the remaining of s if match, else None.

        """
        i = 0
        while i < len(s) and i < len(word):
            if s[i] != word[i]:
                break
            i += 1
        # Match.
        if i == len(word):
            if i == len(s):
                return ''
            else:
                return s[i:]
        # No match.
        else:
            return None


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    s = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab'
    wordDict = ['a', 'aa', 'aaa', 'aaaa', 'aaaaa', 'aaaaaa', 'aaaaaaa',
                'aaaaaaaa', 'aaaaaaaaa', 'aaaaaaaaaa']

    s = 'leetcode'
    wordDict = ['leet', 'code']

    # s = 'applepenapple'
    # wordDict = ['apple', 'pen']

    # s = 'catsandog'
    # wordDict = ['cats', 'dog', 'sand', 'and', 'cat']

    print(Solution().wordBreak(s, wordDict))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    s = 'leetcode'
    wordDict = ['leet', 'code']

    expect = True

    actual = Solution().wordBreak(s, wordDict)

    assert actual == expect


def test_example_2():

    s = 'applepenapple'
    wordDict = ['apple', 'pen']

    expect = True

    actual = Solution().wordBreak(s, wordDict)

    assert actual == expect


def test_example_3():

    s = 'catsandog'
    wordDict = ['cats', 'dog', 'sand', 'and', 'cat']

    expect = False

    actual = Solution().wordBreak(s, wordDict)

    assert actual == expect


def test_long():

    s = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab'
    wordDict = ['a', 'aa', 'aaa', 'aaaa', 'aaaaa', 'aaaaaa', 'aaaaaaa',
                'aaaaaaaa', 'aaaaaaaaa', 'aaaaaaaaaa']

    expect = False

    actual = Solution().wordBreak(s, wordDict)

    assert actual == expect
