###############################################################################
# Filename: test_validate_bst.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_validate_bst.py
#
###############################################################################

import sys


###############################################################################
# Solution
###############################################################################

class TreeNode:
    """
    Definition for a binary tree node.

    """
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:
    def isValidBST(self, root: TreeNode) -> bool:
        """
        Determine whether the tree is a valid binary search tree.

        Notes:
          None.

        Algorithm:
          Perform a recursive DFS tree traversal, checking that each node
          is within the valid range. This range is set by the caller of the
          recursive function. So, each node, after it passes its test,
          it calls the recursive function for the left child and the right
          child, specifying that the max value of the left child has to be
          node value - 1 and that the min value of the right child has to be
          greater than node + 1. The min and max values percolate to the
          bottom of the tree.

        """
        MIN_INT = -sys.maxsize - 1
        MAX_INT = sys.maxsize
        return self._isValidNode(root, MIN_INT, MAX_INT)

    def _isValidNode(self, node, minval, maxval):
        # Base case.
        if not node:
            return True
        # Check node key is within range.
        if node.val < minval or maxval < node.val:
            return False
        return (self._isValidNode(node.left, minval, node.val - 1) and
                self._isValidNode(node.right, node.val + 1, maxval))


###############################################################################
# Test functions
###############################################################################

def binaryTreeFromLevelOrderArray(array):
    return insertLevelOrder(array, 0)


def insertLevelOrder(array, i):
    """
    Construct binary tree from an array of keys.

    Notes:
      - An element equal to None indicates no node at that position in the tree

    """
    # Base case.
    if i >= len(array):
        return None

    # Get node key.
    x = array[i]

    # No node at this location in the tree.
    if not x:
        return None

    # Create node.
    node = TreeNode(x)
    node.left = insertLevelOrder(array, 2 * i + 1)
    node.right = insertLevelOrder(array, 2 * i + 2)

    # Return node.
    return node


def levelOrderArrayFromBinaryTree(root):
    """
    Construct array of keys from binary tree in level order.

    Algorithm:
      Visit the nodes in breadth-first fashion.

    """
    ret = []
    if root:
        queue = [root]
        while queue:
            # Get next node key.
            node = queue.pop(0)
            if node:
                ret.append(node.val)
            else:
                ret.append(None)
            # Add children to the queue.
            if node:
                queue.append(node.left)
            if node:
                queue.append(node.right)

        # Remove trailing Nones.
        i = len(ret) - 1
        while ret[i] is None:
            i -= 1
        ret = ret[:i+1]

    return ret


###############################################################################
# Driver
###############################################################################

def main():
    INPUT = [2, 1, 3]
    # INPUT = [5, 1, 4, None, None, 3, 6]
    INPUT = [10, 5, 15, None, None, 6, 20]
    print(INPUT)
    root = binaryTreeFromLevelOrderArray(INPUT)
    INPUT = levelOrderArrayFromBinaryTree(root)
    print(INPUT)
    print(Solution().isValidBST(root))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():
    """
      2
     / \
    1   3

    """

    INPUT = [2, 1, 3]

    expect = True

    actual = Solution().isValidBST(binaryTreeFromLevelOrderArray(INPUT))

    assert actual == expect


def test_example_2():
    """
      5
     / \
    1   4
       / \
      3   6

    """

    INPUT = [5, 1, 4, None, None, 3, 6]

    expect = False

    actual = Solution().isValidBST(binaryTreeFromLevelOrderArray(INPUT))

    assert actual == expect


def test_other():
    """
      10
     /  \
    5   15
       /  \
      6   20

    """

    INPUT = [10, 5, 15, None, None, 6, 20]

    expect = False

    actual = Solution().isValidBST(binaryTreeFromLevelOrderArray(INPUT))

    assert actual == expect
