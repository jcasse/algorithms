###############################################################################
# Filename: test_removeelement.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_removeelement.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def removeElement(self, nums, val):
        """
        Remove occurrences of val from nums in-place.

        Algorithm:
          Traverse the array from left to right, swapping val elements with
          non-val elements from the right half of the array.
          One index L travels from left to right and another R from right to
          left. Every time the L index encounters an element val, it is
          swapped with an element from R. This repeats until the two indexes
          cross each other.

        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        # This solution works, but defeats the purpose of the exercise.
        # try:
        #     while True:
        #         nums.remove(val)
        # except ValueError:
        #     pass
        # return len(nums)

        # Solution.
        count = 0
        n = len(nums)
        idx_f = 0
        idx_b = len(nums)
        while True:

            # Move backward index to next non-val value.
            while idx_b >= 0:
                idx_b = idx_b - 1
                if idx_b >= 0 and nums[idx_b] != val:
                    break

            # Move forward index to next val and increment count.
            while idx_f < n and nums[idx_f] != val:
                idx_f = idx_f + 1
                count = count + 1

            # Check for stop condition.
            if idx_f > idx_b:
                break

            # Swap element with valid value.
            # Note: Swap not necessary, only assignment.
            nums[idx_f] = nums[idx_b]
            nums[idx_b] = val

        return count


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    nums = [3, 2, 2, 3]
    val = 3
    print(Solution().removeElement(nums, val))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def updatedNums(nums, length):

    # Update nums.
    if length == 0:
        return []
    else:
        return nums[0:length]


def test_simple():

    nums = [3, 2, 2, 3]
    val = 3

    expect = 2

    actual = Solution().removeElement(nums, val)

    assert actual == expect
    assert updatedNums(nums, actual) == [2, 2]


def test_empty():

    nums = []
    val = 3

    expect = 0

    actual = Solution().removeElement(nums, val)

    assert actual == expect
    assert updatedNums(nums, actual) == []


def test_not_present():

    nums = [1, 2, 4, 5]
    val = 3

    expect = 4

    actual = Solution().removeElement(nums, val)

    assert actual == expect
    assert updatedNums(nums, actual) == [1, 2, 4, 5]


def test_only_present():

    nums = [3, 3, 3, 3]
    val = 3

    expect = 0

    actual = Solution().removeElement(nums, val)

    assert actual == expect
    assert updatedNums(nums, actual) == []
