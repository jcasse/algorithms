###############################################################################
# Filename: test_mergetwosortedlists.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_mergetwosortedlists.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def mergeTwoLists(self, l1, l2):
        """
        Merge two sorted linked lists.

        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        # Start new list with first value.
        ret = None
        l1, l2, val = self._nextValue(l1, l2)
        if val is None:  # End of lists.
            return ret
        else:
            ret = ListNode(val)

        # Add the rest of the values to the new list.
        ptr = ret
        while True:

            # Get next value and update list pointers.
            l1, l2, val = self._nextValue(l1, l2)

            # Done.
            if val is None:
                break

            # Add next node.
            ptr.next = ListNode(val)

            # Advance construction pointer.
            ptr = ptr.next

        return ret

    def _nextValue(self, l1, l2):
        """
        Select next smallest value and updated list pointers.

        Args:
          l1 (ListNode): Pointer to current list 1 node.
          l2 (ListNode): Pointer to current list 2 node.

        Returns:
          (ListNode, ListNode, int): Updated list node pointers and value.

        """
        # Find next smallest value and advance corresponding list pointer.
        if l1 and l2:
            if l1.val > l2.val:
                val = l2.val
                l2 = l2.next
            else:
                val = l1.val
                l1 = l1.next
        elif not l1 and not l2:
            val = None
        elif not l1:
            val = l2.val
            l2 = l2.next
        elif not l2:
            val = l1.val
            l1 = l1.next

        # Return smallest value and updated list pointers.
        return l1, l2, val


###############################################################################
# Test functions
###############################################################################

def constructList(array):
    if not array:
        return None
    ret = ListNode(array[0])
    ptr = ret
    for i in range(1, len(array)):
        ptr.next = ListNode(array[i])
        ptr = ptr.next
    return ret


def printList(ls):
    ret = []
    while True:
        if not ls:
            break
        ret.append(ls.val)
        ls = ls.next
    print(ret)


###############################################################################
# Driver
###############################################################################

def main():
    l1 = constructList([1, 2, 4])
    l2 = constructList([1, 3, 4])
    printList(l1)
    printList(l2)
    printList(Solution().mergeTwoLists(l1, l2))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def equalLists(l1, l2):
    while True:

        # Finished both lists without failing any test.
        if not l1 and not l2:
            return True

        # Equal length test.
        if not l1 or not l2:
            return False

        # Equal value test.
        if l1.val != l2.val:
            return False

        # Advance index.
        l1 = l1.next
        l2 = l2.next

    return True


def test_basic():

    l1 = constructList([1, 2, 4])
    l2 = constructList([1, 3, 4])
    printList(l1)
    printList(l2)
    expect = constructList([1, 1, 2, 3, 4, 4])
    printList(expect)

    actual = Solution().mergeTwoLists(l1, l2)

    assert equalLists(actual, expect)


def test_one_empty():

    l1 = constructList([])
    l2 = constructList([0])
    printList(l1)
    printList(l2)
    expect = constructList([0])
    printList(expect)

    actual = Solution().mergeTwoLists(l1, l2)
    printList(actual)

    assert equalLists(actual, expect)


def test_negative_values():

    l1 = constructList([-10, -9, -6, -4, 1, 9, 9])
    l2 = constructList([-5, -3, 0, 7, 8, 8])
    printList(l1)
    printList(l2)
    expect = constructList([-10, -9, -6, -5, -4, -3, 0, 1, 7, 8, 8, 9, 9])
    printList(expect)

    actual = Solution().mergeTwoLists(l1, l2)
    printList(actual)

    assert equalLists(actual, expect)
