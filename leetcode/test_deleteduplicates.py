###############################################################################
# Filename: test_deleteduplicates.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_deleteduplicates.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class ListNode:
    """
    Definition for singly-linked list node.
    """
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        """
        Delete duplicates from the linked list.

        Notes:

          - The list is sorted.

        Algorithm:

          Maintain the current distinct node value. Traverse the list,
          removing nodes with same value as current, and updating current
          when we encounter a different value along the way.

        Pseudo code:

          while node.next:
            if next.val == current:
              delete node
            else:
              current == next.val
              node = next

        """
        if head:
            current = head
            while current.next:
                if current.next.val == current.val:
                    current.next = current.next.next
                else:
                    current = current.next

        return head


###############################################################################
# Test functions
###############################################################################

def linkedListFromArray(array):
    if not array:
        return None
    head = ListNode(array[0])
    pointer = head
    for i in range(1, len(array)):
        pointer.next = ListNode(array[i])
        pointer = pointer.next
    return head


def arrayFromLinkedList(head):
    array = []
    while head:
        array.append(head.val)
        head = head.next
    return array


###############################################################################
# Driver
###############################################################################

def main():
    head = linkedListFromArray([1, 1, 2])
    print(arrayFromLinkedList(head))
    print(arrayFromLinkedList(Solution().deleteDuplicates(head)))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    head = linkedListFromArray([1, 1, 2])

    expect = [1, 2]

    actual = arrayFromLinkedList(Solution().deleteDuplicates(head))

    assert actual == expect


def test_example_2():

    head = linkedListFromArray([1, 1, 2, 3, 3])

    expect = [1, 2, 3]

    actual = arrayFromLinkedList(Solution().deleteDuplicates(head))

    assert actual == expect


def test_empty():

    head = linkedListFromArray([])

    expect = []

    actual = arrayFromLinkedList(Solution().deleteDuplicates(head))

    assert actual == expect
