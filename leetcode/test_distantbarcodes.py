###############################################################################
# Filename: test_distantbarcodes.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_distantbarcodes.py
#
###############################################################################

from typing import List


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def rearrangeBarcodes(self, barcodes: List[int]) -> List[int]:
        """
        Rearrange barcodes so that no one appears consecutive in the array.

        Notes:
          - 1 <= barcodes.length <= 10000
          - 1 <= barcodes[i] <= 10000
          - There is always a solution

        Intuition:
          In the worst case, we can have (N + 1) / 2 occurrences of the same
          barcode.

        Algorithm:
          In order of most occurrences, places bracodes in alternating
          positions.

        """
        # Compute occurrences of barcodes.
        counts = {}
        for x in barcodes:
            if x in counts:
                counts[x] += 1
            else:
                counts[x] = 1

        # Sort in order of decreasing count.
        counts = sorted(counts.items(), key=lambda item: item[1], reverse=True)

        # Create array of barcodes in decreasing order of occurrences.
        occurrences = []
        for number, count in counts:
            for i in range(count):
                occurrences.append(number)

        # Initialize output array.
        ret = [None] * len(barcodes)

        # Place barcodes in alternating positions.
        i = 0
        for number in occurrences:
            # Place next barcode number in alternating position.
            ret[i] = number
            # Skip to next alternating position.
            i += 2
            # Wrap around.
            if i >= len(barcodes):
                i = 1

        return ret


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    barcodes = [7, 7, 7, 8, 5, 7, 5, 5, 5, 8]
    print(barcodes)
    print(Solution().rearrangeBarcodes(barcodes))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    barcodes = [1, 1, 1, 2, 2, 2]
    print(barcodes)

    expect = [1, 2, 1, 2, 1, 2]

    actual = Solution().rearrangeBarcodes(barcodes)
    print(actual)

    assert actual == expect


def test_example_2():
    barcodes = [1, 1, 1, 1, 2, 2, 3, 3]
    print(barcodes)

    expect = [1, 2, 1, 2, 1, 3, 1, 3]

    actual = Solution().rearrangeBarcodes(barcodes)
    print(actual)

    assert actual == expect


def test_short():
    barcodes = [1, 1, 2]
    print(barcodes)

    expect = [1, 2, 1]

    actual = Solution().rearrangeBarcodes(barcodes)
    print(actual)

    assert actual == expect


def test_other():
    barcodes = [7, 7, 7, 8, 5, 7, 5, 5, 5, 8]
    print(barcodes)

    expect = [7, 5, 7, 5, 7, 5, 7, 8, 5, 8]

    actual = Solution().rearrangeBarcodes(barcodes)
    print(actual)

    assert actual == expect
