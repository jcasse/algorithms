###############################################################################
# Filename: test_container_with_most_water.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line:
# pytest test_container_with_most_water.py -v
#
###############################################################################


from typing import List


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def maxArea(self, height: List[int]) -> int:
        """
        Find largest area.

        The list of heights encode vertical lines along the x-axis. Finds the
        maximum area formed.

        Example:
            Input: [1, 8, 6, 2, 5, 4, 8, 3, 7]
            Output: 49
            Explanation: Area between lines at in indices 1 and 8 with height
            7 = 49.

        Algorithm:
            Starting at height 1, for each height compute the maximum area.
            The maximum area at each height is computed by finding the
            leftmost and rightmost lines that cover that height and computing
            the area: (right index - left index) * height. Each max area runs
            in O(n). So, the worst case time complexity is O(n * max(height)).

        """
        maxArea = 0
        maxHeight = max(height)
        for h in range(1, maxHeight):
            area = self.getMaxArea(height, h)
            # If no pair of lines at this height, we are done.
            if not area:
                break
            maxArea = area if area > maxArea else maxArea
        return maxArea

    def getMaxArea(self, array, h):
        # Initialize.
        leftpos = None
        rightpos = None

        # Find first and last lines.
        for i in range(len(array)):
            if array[i] >= h:
                if not leftpos:
                    leftpos = i   # first line covering this height
                else:
                    rightpos = i  # last line covering this height

        # Check if area exists at height h.
        if not leftpos or not rightpos:
            return None

        # Return area.
        return (rightpos - leftpos) * h


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################


def main():

    height = [1, 8, 6, 2, 5, 4, 8, 3, 7]

    print(Solution().maxArea(height))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################


def test_example_1():

    height = [1, 8, 6, 2, 5, 4, 8, 3, 7]

    expect = 49

    actual = Solution().maxArea(height)

    assert actual == expect


def test_test_case_21_of_50():

    height = [1, 1]

    expect = 1

    actual = Solution().maxArea(height)

    assert actual == expect
