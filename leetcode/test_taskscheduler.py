###############################################################################
# Filename: test_taskscheduler.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest -v test_taskscheduler.py
#
###############################################################################

import sys
import collections
from typing import List

import pytest


###############################################################################
# Solution
###############################################################################

class Task:
    def __init__(self, symbol, count):
        self.symbol = symbol
        self.count = count

    @staticmethod
    def get_sorting_key(task):
        return task.count


class Solution(object):
    def leastInterval(self, tasks: List[str], n: int) -> int:
        """
        Determine least number of intervals.

        Determines the least number of intervals to execute all tasks in list.
        Tasks can be scheduled in any order. For each interval, the CPU can
        either do one task or be idle.

        Notes:
          - Each task is named as A-Z
          - Each task is completed in one interval
          - n is the number of intervals between tasks of same type (letter)
          - The number of tasks is in the range [1, 10000]
          - The integer n is in the range [0, 100]

        Example:
          Input: tasks = ["A","A","A","B","B","B"], n = 2
          Output: 8
          Explanation: A -> B -> idle -> A -> B -> idle -> A -> B.

        Intuition:
            Group heterogeneous tasks together so that on idle interval can be
            used for all in the group at once.

        Algorithm:
            1. Create hash map: task-type -> count
            2. Schedule tasks in groups, with cooling interval between groups.
            The cooling interval is calculated as the minimum needed to satisfy
            the condition for all tasks in the group.

        """
        ret: int = 0

        # Sort tasks by count.
        counts = collections.defaultdict(int)
        for task in tasks:
            counts[task] += 1
        tasks = []
        for task, count in counts.items():
            tasks.append(Task(task, count))
        tasks.sort(key=Task.get_sorting_key, reverse=True)
        for task in tasks:
            print(task.symbol, task.count)

        more = True
        while more:

            count = 0
            more = False

            # Schedule tasks.
            for task in tasks:

                # If any task has more than 1, there will be more for a
                # next iteration.
                if task.count > 1:
                    more = True

                # Schedule one of each task, in order, until cooling is
                # required.
                if count < n + 1:
                    if task.count > 0:
                        ret += 1
                        task.count -= 1
                        count += 1

                # If any task has more than 1, there will be more for a
                # next iteration.
                if task.count > 0:
                    more = True

            # If more tasks pending, add cooling and sort tasks according to
            # their updated counts.
            if more:
                cooling = n - count + 1
                ret += cooling
                tasks.sort(key=Task.get_sorting_key, reverse=True)

        return ret


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():

    with open('data/taskscheduler/test_case_58.txt') as fd:
        lines = fd.readlines()

    tasks = lines[0].strip().replace('"', '')
    tasks = tasks.strip('][').split(',')
    n = int(lines[1].strip())

    print('tasks = {}'.format(tasks))
    print('n = {}'.format(n))

    print(Solution().leastInterval(tasks, n))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    tasks = ['A', 'A', 'A', 'B', 'B', 'B']
    n = 2

    expect = 8

    actual = Solution().leastInterval(tasks, n)

    assert actual == expect


def test_case_21_of_64():

    tasks = ['A', 'A', 'A', 'A', 'A', 'A', 'B', 'C', 'D', 'E', 'F', 'G']
    n = 2

    expect = 16

    actual = Solution().leastInterval(tasks, n)

    assert actual == expect


def test_case_58_of_64():

    with open('data/taskscheduler/test_case_58.txt') as fd:
        lines = fd.readlines()
    tasks = lines[0].strip().strip('[]').replace('"', '').split(',')
    n = int(lines[1].strip())

    expect = 1000

    actual = Solution().leastInterval(tasks, n)

    assert actual == expect


def test_case_60_of_64():

    tasks = ['A', 'A', 'A', 'B', 'B', 'B']
    n = 0

    expect = 6

    actual = Solution().leastInterval(tasks, n)

    assert actual == expect
