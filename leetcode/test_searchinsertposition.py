###############################################################################
# Filename: test_searchinsertposition.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_searchinsertposition.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        index = 0
        while index < len(nums):
            if target <= nums[index]:
                break
            index = index + 1
        return index


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    nums = [1, 3, 5, 6]
    target = 5
    print(Solution().searchInsert(nums, target))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_simple_2():

    nums = [1, 3, 5, 6]
    target = 5

    expect = 2

    actual = Solution().searchInsert(nums, target)

    assert actual == expect


def test_simple_1():

    nums = [1, 3, 5, 6]
    target = 2

    expect = 1

    actual = Solution().searchInsert(nums, target)

    assert actual == expect


def test_first():

    nums = [1, 3, 5, 6]
    target = 0

    expect = 0

    actual = Solution().searchInsert(nums, target)

    assert actual == expect


def test_last():

    nums = [1, 3, 5, 6]
    target = 7

    expect = 4

    actual = Solution().searchInsert(nums, target)

    assert actual == expect
