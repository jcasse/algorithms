###############################################################################
# Filename: test_validanagram.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_validanagram.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def isAnagram(self, s: str, t: str) -> bool:
        """
        Determine whether t is an anagram of s.

        An anagram is a rearrangement of letters.

        Notes:
          - The strings contain only lowercase alphabets

        Algorithm:
          Simply count the occurrences of letters in both words and check that
          they match.

        """
        counts = {}
        for char in s:
            if char in counts:
                counts[char] += 1
            else:
                counts[char] = 1
        for char in t:
            if char not in counts:
                return False
            counts[char] -= 1
            if counts[char] == 0:
                del counts[char]
        if len(counts) > 0:
            return False
        return True


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    s = "anagram"
    t = "nagaram"

    print(Solution().isAnagram(s, t))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    s = 'anagram'
    t = 'nagaram'

    expect = True

    actual = Solution().isAnagram(s, t)

    assert actual == expect


def test_example_2():

    s = 'rat'
    t = 'car'

    expect = False

    actual = Solution().isAnagram(s, t)

    assert actual == expect
