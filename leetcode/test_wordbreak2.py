###############################################################################
# Filename: test_wordbreak2.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_wordbreak2.py
#
###############################################################################

from typing import List, Union, Dict


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def wordBreak(self, s: str, wordDict: List[str]) -> List[str]:
        """
        Find all possible segmentations of s given words in dictionary.

        Notes:
          - The word s is non-empty
          - The dictionary contains non-empty words
          - The dictionary does not contain duplicate words
          - Words in the dictionary may be reused

        Intuition:
          Recursive algorithm with memoization (dynamic programming).
          Consider all possible matches by finding solutions to smaller
          problems, remembering those solutions to avoid repeating computation.

        Algorithm:
          Starting with the full word as the root of the recursive tree,
          find all possible matches of the beginning of the word.
          Build a solution string as you traverse each branch. Memoize
          solutions to sub-problems as a List[str]. An empty list indicates
          no solutions for sub-problem.
          When a sub-problem has already been solved, concatenate the partial
          solution so far with the sup-solution to the sub-problem.

        """
        memo = {}       # s -> List[str] solutions to sub-problem 's'
        solutions = []  # List[str] all solutions
        partial_soln = ''
        self._recursiveWordBreak(s, wordDict, partial_soln, memo, solutions)
        return solutions

    def _recursiveWordBreak(self, s: str, wordDict: List[str],
                            partial_soln: str,
                            memo: Dict[str, List[str]],
                            solutions: List[str]) -> List[str]:

        this_sub_solns = []

        sub_solns = memo.get(s)

        # If no known sub-problem, compute.
        if sub_solns is None:

            # Solve sub-problem.
            solved = False
            for word in wordDict:

                # Match word with beginning of string s.
                remainder = self._matchPrefix(s, word)

                # Base case: Entire string segmented. Add partial_soln.
                if remainder == '':
                    solved = True
                    run_soln = partial_soln + ' ' + word if partial_soln else word
                    solutions.append(run_soln)
                    this_sub_solns.append(word)

                    # Update memo to contain sub-solutions to sub-problem s.
                    sub_solns = memo.get(s)
                    if sub_solns:
                        sub_solns.append(word)
                    else:
                        memo[s] = [word]

                # Continue recursive segmentation.
                elif remainder:
                    run_soln = partial_soln + ' ' + word if partial_soln else word
                    sub_solns = self._recursiveWordBreak(remainder, wordDict,
                                                         run_soln,
                                                         memo, solutions)
                    # Update memo.
                    if sub_solns:
                        memos = memo.get(s)
                        if memos:
                            for sub_soln in sub_solns:
                                memos.append(sub_soln)
                        solved = True

                # No match.
                else:
                    pass

            # Mark as dead end. No solutions from this point forward.
            if not solved:
                memo[s] = []

        # If sub-problem already solved, merge sub-solutions to partial_soln.
        else:
            for sub_soln in sub_solns:
                solution = partial_soln + ' ' + sub_soln if partial_soln else sub_soln
                solutions.append(solution)

        # Return the sub_solutions for this sub_problem.
        return this_sub_solns

    def _matchPrefix(self, s: str, word: List[str]) -> Union[str, None]:
        """
        Determine if word is a prefix of s.

        Returns the remaining of s if match, else None.

        """
        i = 0
        while i < len(s) and i < len(word):
            if s[i] != word[i]:
                break
            i += 1
        # Match.
        if i == len(word):
            if i == len(s):
                return ''
            else:
                return s[i:]
        # No match.
        else:
            return None


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    s = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab'
    wordDict = ['a', 'aa', 'aaa', 'aaaa', 'aaaaa', 'aaaaaa', 'aaaaaaa',
                'aaaaaaaa', 'aaaaaaaaa', 'aaaaaaaaaa']

    s = 'leetcode'
    wordDict = ['leet', 'code']

    s = 'applepenapple'
    wordDict = ['apple', 'pen']

    s = 'catsandog'
    wordDict = ['cats', 'dog', 'sand', 'and', 'cat']

    s = 'catsanddog'
    wordDict = ['cat', 'cats', 'and', 'sand', 'dog']

    s = 'pineapplepenapple'
    wordDict = ['apple', 'pen', 'applepen', 'pine', 'pineapple']

    s = 'catsandog'
    wordDict = ['cats', 'dog', 'sand', 'and', 'cat']

    s = 'aaaaaaa'
    wordDict = ['aaaa', 'aa', 'a']

    print(Solution().wordBreak(s, wordDict))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    s = 'catsanddog'
    wordDict = ['cat', 'cats', 'and', 'sand', 'dog']

    expect = ['cat sand dog', 'cats and dog']

    actual = Solution().wordBreak(s, wordDict)

    assert actual == expect


def test_example_2():

    s = 'pineapplepenapple'
    wordDict = ['apple', 'pen', 'applepen', 'pine', 'pineapple']

    expect = ['pine apple pen apple', 'pine applepen apple',
              'pineapple pen apple']

    actual = Solution().wordBreak(s, wordDict)

    assert actual == expect


def test_example_3():

    s = 'catsandog'
    wordDict = ['cats', 'dog', 'sand', 'and', 'cat']

    expect = []

    actual = Solution().wordBreak(s, wordDict)

    assert actual == expect


def test_long():

    s = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab'
    wordDict = ['a', 'aa', 'aaa', 'aaaa', 'aaaaa', 'aaaaaa', 'aaaaaaa',
                'aaaaaaaa', 'aaaaaaaaa', 'aaaaaaaaaa']

    expect = []

    actual = Solution().wordBreak(s, wordDict)

    assert actual == expect


def test_hard():

    s = 'aaaaaaa'
    wordDict = ['aaaa', 'aa', 'a']

    expect = ['a a a a a a a', 'aa a a a a a', 'a aa a a a a', 'a a aa a a a',
              'aa aa a a a', 'aaaa a a a', 'a a a aa a a', 'aa a aa a a',
              'a aa aa a a', 'a aaaa a a', 'a a a a aa a', 'aa a a aa a',
              'a aa a aa a', 'a a aa aa a', 'aa aa aa a', 'aaaa aa a',
              'a a aaaa a', 'aa aaaa a', 'a a a a a aa', 'aa a a a aa',
              'a aa a a aa', 'a a aa a aa', 'aa aa a aa', 'aaaa a aa',
              'a a a aa aa', 'aa a aa aa', 'a aa aa aa', 'a aaaa aa',
              'a a a aaaa', 'aa a aaaa', 'a aa aaaa']

    actual = Solution().wordBreak(s, wordDict)

    assert actual == expect
