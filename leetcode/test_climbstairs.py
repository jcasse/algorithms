###############################################################################
# Filename: test_climbstairs.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_climbstairs.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def climbStairs(self, n: int) -> int:

        """
        Compute number of ways to climb n steps stepping either 1 or 2 steps.

        Notes:
          - n is positive
          - Other solutions:
            https://leetcode.com/problems/climbing-stairs/solution

        Algorithm:
          Every time a 2 is added to the steps, the total number of steps is
          reduced by 1. Compute the number of ways to choose 2s.
          for each number of 2s, compute n choose n - #2s and add it to the
          total.

        """
        ret = 0
        for i in range(n // 2 + 1):
            ret += self._nChooseK(n - i, i)
        return ret

    def _nChooseK(self, n, k):
        """
        Compute n choose k.

        Algorithm:
          Exploits symmetry and cancellation of terms.
          1. Compute ((n) * (n - 1) * ... * (k + 1)) / (n - k)!

        """
        if n == 0:
            return 0
        numerator = 1
        for i in range(n, k, -1):
            numerator *= i
        denominator = 1
        for i in range(n - k, 0, -1):
            denominator *= i
        return int(numerator / denominator)


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    n = 0
    print(Solution().climbStairs(n))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_0():

    n = 0

    expect = 0

    actual = Solution().climbStairs(n)

    assert actual == expect


def test_1():

    n = 1

    expect = 1

    actual = Solution().climbStairs(n)

    assert actual == expect


def test_2():

    n = 2

    expect = 2

    actual = Solution().climbStairs(n)

    assert actual == expect


def test_3():

    n = 3

    expect = 3

    actual = Solution().climbStairs(n)

    assert actual == expect


def test_4():

    n = 4

    expect = 5

    actual = Solution().climbStairs(n)

    assert actual == expect


def test_5():

    n = 5

    expect = 8

    actual = Solution().climbStairs(n)

    assert actual == expect
