###############################################################################
# Filename: test_validparentheses.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_validparentheses.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        """
        Compute the length of the last word in the string s.

        Notes:
          - The string s consists only of alphabets and space
          - If last word does not exist, return 0

        """
        ret: int = 0
        s = s.rstrip()
        for i in range(len(s)):
            if s[i] != ' ':
                ret += 1
            else:
                ret = 0
        return ret


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    print(Solution().lengthOfLastWord('Hello World'))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_basic():

    s = 'Hello World'

    expect = 5

    actual = Solution().lengthOfLastWord(s)

    assert actual == expect


def test_trailing_space():

    s = 'a '

    expect = 1

    actual = Solution().lengthOfLastWord(s)

    assert actual == expect
