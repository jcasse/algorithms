###############################################################################
# Filename: test_decodeways.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_decodeways.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def numDecodings(self, s: str) -> int:
        """
        Compute number of ways to decode a string of digits.

        The string of digits is an encoding of letters:
          A -> 1, B -> 2, ... Z -> 26.

        Notes:
          - The string is non-empty
          - Each character in the string is a digit [0-9]
          - Recursive Solution Explanation:
            https://www.youtube.com/watch?v=YcJTyrG3bZs

        Algorithm:
          Each character can be decoded as a single character or together with
          the next character, if the two characters are not greater than 26.
          Starting at the first character, we decode away that character and
          possibly the next character together with it. We repeat this
          procedure recursively and implicitly construct a binary of all the
          possible decodings, denoted by each path to a leaf. Since the tree
          has 2^n nodes (computations), we memoize solutions to subproblems
          (nodes) so that we can prune subtrees.

        """
        # Dynamic programming solution.
        # # Initialize.
        # n = len(s)
        # dp = [None] * (n + 1)
        # # Base cases.
        # dp[0] = 1  # Starting point
        # dp[1] = 1  # Number of ways for string of length 1
        # for i in range(2, n + 1):
        #     value = int(s[i-1: i])
        #     one = dp[i-1] if value >= 0 else 0
        #     two = dp[i-2] if value >= 10 and value <= 26 else 0
        #     dp[i] = one + two

        # return dp[n]

        # Recursive solution with memoization.
        subproblems = [None] * len(s)
        return self._numDecodings(s, 0, subproblems)

    def _numDecodings(self, s, i, subproblems):

        # Base case.
        # We have reached the end of the string.
        if i == len(s):
            return 1

        # Sub-problem already solved.
        if subproblems[i]:
            return subproblems[i]

        # Compute answer to new sub-problem.
        decodeWays = 0
        if i + 1 <= len(s):
            if self._isValid(s[i: i + 1]):
                decodeWays += self._numDecodings(s, i + 1, subproblems)
        if i + 2 <= len(s):
            if self._isValid(s[i: i + 2]):
                decodeWays += self._numDecodings(s, i + 2, subproblems)

        # Memoize the answer to new sub-problem.
        subproblems[i] = decodeWays

        return subproblems[i]

    def _isValid(self, s):
        if not s:
            return False
        if s[0] == '0':
            return False
        value = int(s)
        return value >= 1 and value <= 26


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    s = '12'
    print(Solution().numDecodings(s))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    s = '12'

    expect = 2

    actual = Solution().numDecodings(s)

    assert actual == expect


def test_example_2():

    s = '226'

    expect = 3

    actual = Solution().numDecodings(s)

    assert actual == expect


def test_complex():

    s = '226326'

    expect = 6

    actual = Solution().numDecodings(s)

    assert actual == expect


def test_invalid_digit():

    s = '01'

    expect = 0

    actual = Solution().numDecodings(s)

    assert actual == expect
