###############################################################################
# Filename: test_interleavingstring.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_interleavingstring.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def isInterleave(self, s1: str, s2: str, s3: str) -> bool:
        """
        Determine whether s3 is formed by the interleaving s1 and s2.

        Notes:
          None.

        Algorithm:
          Since they are interleaved, the order of letters is preserved.
          We can inspect the letters in s3, in order, checking off a
          matching letter from either s1 or s2. Since there is a binary
          choice at each step, this forms an implicit binary tree with
          2^n nodes (possible inspections). If s3 is the interleaving of
          s1 and s2, there is at least one branch of the tree that represents
          the interleaving. A successful interleaving is one where all three
          strings are traversed to the end, matching all letters.
          Depth-first search with memoization will work.
          A node in the tree consists of the three strings and the current
          indexes into each one.

          To make the algorithm efficient, we need to prune long branches
          that lead nowhere.
          These long branches are created when the character match is
          consecutively done only on the same s1 or s2.
          This is easier to implement in the recursive DFS algorithm, where
          we mark a node as failed if it only matched the current character
          to only one of the s1 or s2.

        """
        # DFS ITERATIVE VERSION without LONG BRANCH PRUNING.
        # failedSubproblems = set()  # Set of branches that fail (prune them)
        # stack = [(0, 0, 0)]        # Initialize to first node.
        # while stack:

        #     # Get next node in the depth-first search order.
        #     (i1, i2, i3) = stack.pop()
        #     print(i1, i2, i3)

        #     # Prune branch if sub-problem already solved.
        #     if (i1, i2, i3) in failedSubproblems:
        #         continue

        #     # Interleaving exists.
        #     if i1 == len(s1) and i2 == len(s2) and i3 == len(s3):
        #         return True

        #     # The output string s3 is too long. Mark branch as failed.
        #     if i3 == len(s3):
        #         failedSubproblems.add((i1, i2, i3))
        #         continue

        #     # Find next matching character in both s1 and s2.
        #     found = 0
        #     if i1 < len(s1) and s1[i1] == s3[i3]:
        #         print('append {} {} {}'.format(i1 + 1, i2, i3 + 1))
        #         stack.append((i1 + 1, i2, i3 + 1))
        #         found += 1
        #     if i2 < len(s2) and s2[i2] == s3[i3]:
        #         print('append {} {} {}'.format(i1, i2 + 1, i3 + 1))
        #         stack.append((i1, i2 + 1, i3 + 1))
        #         found += 1

        #     # No matching characters found. Mark branch as failed.
        #     if found == 0:
        #         failedSubproblems.add((i1, i2, i3))

        # # No path exists to an interleaving leaf.
        # return False

        # DFS RECURSIVE VERSION with LONG BRANCH PRUNING.
        failedSubproblems = set()
        return self._isInterleave(s1, s2, s3, 0, 0, 0, failedSubproblems)

    def _isInterleave(self, s1, s2, s3, i1, i2, i3, failedSubproblems):

        # Prune branch if sub-problem already solved.
        if (i1, i2, i3) in failedSubproblems:
            return False

        # Interleaving exists.
        if i1 == len(s1) and i2 == len(s2) and i3 == len(s3):
            return True

        # The output string s3 is too long.
        if i3 == len(s3):
            failedSubproblems.add((i1, i2, i3))
            return False

        # Find next matching character in both s1 and s2.
        charMatched = 0
        if i1 < len(s1) and s1[i1] == s3[i3]:
            charMatched += 1
            result = self._isInterleave(s1, s2, s3,
                                        i1 + 1, i2, i3 + 1, failedSubproblems)
            if result is True:
                return True
        if i2 < len(s2) and s2[i2] == s3[i3]:
            charMatched += 1
            result = self._isInterleave(s1, s2, s3,
                                        i1, i2 + 1, i3 + 1, failedSubproblems)
            if result is True:
                return True

        # Single-child node failed. Mark this node as failed.
        # This improves time complexity by orders of magnitude.
        if charMatched == 1:
            failedSubproblems.add((i1, i2, i3))

        # No path exists to an interleaving leaf.
        return False


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    s1 = 'aabcc'
    s2 = 'dbbca'
    s3 = 'aadbbcbcac'

    print(Solution().isInterleave(s1, s2, s3))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():
    s1 = 'aabcc'
    s2 = 'dbbca'
    s3 = 'aadbbcbcac'

    expect = True

    actual = Solution().isInterleave(s1, s2, s3)

    assert actual == expect


def test_example_2():

    s1 = 'aabcc'
    s2 = 'dbbca'
    s3 = 'aadbbbaccc'

    expect = False

    actual = Solution().isInterleave(s1, s2, s3)

    assert actual == expect


def test_empty():

    s1 = ''
    s2 = ''
    s3 = ''

    expect = True

    actual = Solution().isInterleave(s1, s2, s3)

    assert actual == expect


def test_empty_s1():

    s1 = ''
    s2 = 'aabcdd'
    s3 = 'aabcdd'

    expect = True

    actual = Solution().isInterleave(s1, s2, s3)

    assert actual == expect


def test_shorter_s3():

    s1 = 'a'
    s2 = 'b'
    s3 = 'a'

    expect = False

    actual = Solution().isInterleave(s1, s2, s3)

    assert actual == expect


def test_longer_s3():

    s1 = 'a'
    s2 = 'b'
    s3 = 'abb'

    expect = False

    actual = Solution().isInterleave(s1, s2, s3)

    assert actual == expect


def test_concatenation():

    s1 = 'aabc'
    s2 = 'abad'
    s3 = 'aabcabad'

    expect = True

    actual = Solution().isInterleave(s1, s2, s3)

    assert actual == expect


def test_scale():

    s1 = 'bbbbbabbbbabaababaaaabbababbaaabbabbaaabaaaaababbbababbbbbabbbbababbabaabababbbaabababababbbaaababaa'
    s2 = 'babaaaabbababbbabbbbaabaabbaabbbbaabaaabaababaaaabaaabbaaabaaaabaabaabbbbbbbbbbbabaaabbababbabbabaab'
    s3 = 'babbbabbbaaabbababbbbababaabbabaabaaabbbbabbbaaabbbaaaaabbbbaabbaaabababbaaaaaabababbababaababbababbbababbbbaaaabaabbabbaaaaabbabbaaaabbbaabaaabaababaababbaaabbbbbabbbbaabbabaabbbbabaaabbababbabbabbab'

    expect = False

    actual = Solution().isInterleave(s1, s2, s3)

    assert actual == expect
