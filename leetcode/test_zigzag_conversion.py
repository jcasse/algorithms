###############################################################################
# Filename: test_zigzag_conversion.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_zigzag_conversion.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def convert(self, s: str, numRows: int) -> str:
        """
        Convert string s to zigzag pattern.

        Example:
            Input: s = "PAYPALISHIRING" numRows = 2
            Output: "PAHNAPLSIIGYIR"
            Explanation:
            P   A   H   N
            A P L S I I G
            Y   I   R

        Algorithm:
            Distribute letters in s into numRows arrays in zigzagging order.
            Concatenate arrays.

        """
        # Initialize.
        rows = [[] for i in range(numRows)]
        zigzagger = Zigzagger(0, numRows)

        # Convert.
        for char in s:
            index = zigzagger.getIndex()
            rows[index].append(char)
        ret = ''
        for row in rows:
            ret += ''.join(row)
        return ret


class Zigzagger():
    def __init__(self, beg, end):
        self.beg = beg
        self.end = end
        self.i = self.beg
        self.forward = True

    def getIndex(self):

        ret = self.i

        # Reset direction.
        if self.i == 0:
            self.forward = True
        elif self.i == self.end - 1:
            self.forward = False
        else:
            pass

        # Iterate next.
        if self.forward and self.i < self.end - 1:
            self.i += 1
        if not self.forward and self.i > self.beg:
            self.i -= 1

        return ret

###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################


def main():

    s = 'PAYPALISHIRING'
    numRows = 3

    print(Solution().convert(s, numRows))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################


def test_empty_s():

    s = ''
    numRows = 0

    expect = ''

    actual = Solution().convert(s, numRows)

    assert actual == expect


def test_example_1():

    s = 'PAYPALISHIRING'
    numRows = 3

    expect = 'PAHNAPLSIIGYIR'

    actual = Solution().convert(s, numRows)

    assert actual == expect


def test_example_2():

    s = 'PAYPALISHIRING'
    numRows = 4

    expect = 'PINALSIGYAHRPI'

    actual = Solution().convert(s, numRows)

    assert actual == expect
