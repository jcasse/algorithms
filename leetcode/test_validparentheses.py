###############################################################################
# Filename: test_validparentheses.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_validparentheses.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def isValid(self, s):
        """
        Check if bracket sequence is valid.

        Notes:
          - Empty string s is a valid sequence

        :type s: str
        :rtype: bool
        """
        # Parenthesis mapping.
        RIGHT_LEFT = {'(': ')', '{': '}', '[': ']'}

        # Perform check.
        stack = self._Stack()
        for char in s:
            # Push left parenthesis into stack.
            if char in RIGHT_LEFT.keys():
                stack.push(char)
            # Check right parenthesis against matching left parenthesis.
            else:
                if stack.empty():
                    return False
                if char != RIGHT_LEFT[stack.pop()]:  # matching parenthesis
                    return False

        # At this point, stack must be empty.
        return stack.empty()

    class _Stack():
        def __init__(self):
            self.container = []

        def push(self, char):
            self.container.append(char)

        def pop(self):
            return self.container.pop()

        def empty(self):
            return not self.container


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    print(Solution().isValid('()[]{}{()[]{}}'))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_empty():

    parens = ''
    expect = True

    actual = Solution().isValid(parens)

    assert actual == expect


def test_one_left_paren():

    parens = '('
    expect = False

    actual = Solution().isValid(parens)

    assert actual == expect


def test_one_rigt_paren():

    parens = ')'
    expect = False

    actual = Solution().isValid(parens)

    assert actual == expect


def test_simple_valid():

    parens = '()'
    expect = True

    actual = Solution().isValid(parens)

    assert actual == expect


def test_nested_valid():

    parens = '([])'
    expect = True

    actual = Solution().isValid(parens)

    assert actual == expect


def test_simple_invalid():

    parens = '(]'
    expect = False

    actual = Solution().isValid(parens)

    assert actual == expect


def test_interleaved():

    parens = '([)]'
    expect = False

    actual = Solution().isValid(parens)

    assert actual == expect


def test_sequence_valid():

    parens = '()[]'
    expect = True

    actual = Solution().isValid(parens)

    assert actual == expect


def test_nested_sequence():

    parens = '()[]{}{()[]{}}'
    expect = True

    actual = Solution().isValid(parens)

    assert actual == expect
