###############################################################################
# Filename: test_plusone.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_plusone.py
#
###############################################################################

from typing import List, Tuple


###############################################################################
# Solution
###############################################################################

class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        """
        Add 1 to the non-negative integer represented by the list.

        Notes:
          - The array is non-empty
          - Each element of the array is a single digit
          - The array does not start with 0 unless it represents the number 0

        Algorithm:
          1. Add 1 to the right-most (least significant) digit
          2. Append resulting digit to new array
          3. Repeat 1 and 2 for any carry over to next digit to the left
          4. Reverse new array

        Pseudo code:
          for x in reversed(digits):
              result, carry = add(carry + x)
              new_array.append(result)
          return reversed(new_array)

        """
        ret = []
        carry = 1
        for x in reversed(digits):
            carry, res = self._add(carry, x)
            ret.append(res)
        if carry:
            ret.append(carry)
        ret.reverse()
        return ret

        # Pythonic solution.
        # return [int(x) for x in str(int(''.join(str(x) for x in digits))+1)]

    def _add(self, carry: int, x: int) -> Tuple[int, int]:
        addition = carry + x
        if addition // 10 == 1:  # carry over
            carry = 1
            digit = addition - 10
        else:  # no carry over
            carry = 0
            digit = addition
        return carry, digit


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    digits = [1, 2, 3, 4]
    print(digits)
    print(Solution().plusOne(digits))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    digits = [1, 2, 3]

    expect = [1, 2, 4]

    actual = Solution().plusOne(digits)

    assert actual == expect


def test_example_2():

    digits = [4, 3, 2, 1]

    expect = [4, 3, 2, 2]

    actual = Solution().plusOne(digits)

    assert actual == expect


def test_single_digit_carry():

    digits = [9]

    expect = [1, 0]

    actual = Solution().plusOne(digits)

    assert actual == expect


def test_single_digit_no_carry():

    digits = [0]

    expect = [1]

    actual = Solution().plusOne(digits)

    assert actual == expect
