###############################################################################
# Filename: test_sqrtx.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_sqrtx.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def mySqrt(self, x: int) -> int:
        """
        Find the square root of x.

        Notes:
          - x is a non-negative integer
          - Only compute the integer part of the square root

        Algorithm:
          1. Starting with 0, repeatedly compute the square of each integer
             and compare to x
          2. Stop when the square is greater than x
          3. The answer is the previous integer

        Pseudo Code:
          ret = 0
          while True:
              nextInt = ret + 1
              if nextInt * nextInt > x:
                  return ret
              ret = nextInt
          return ret

        """
        ret = 0
        while True:
            nextInt = ret + 1
            if nextInt * nextInt > x:
                return ret
            ret = nextInt
        return ret


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    x = 8
    print(Solution().mySqrt(x))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_0():

    x = 0

    expect = 0

    actual = Solution().mySqrt(x)

    assert actual == expect


def test_4():

    x = 4

    expect = 2

    actual = Solution().mySqrt(x)

    assert actual == expect


def test_8():

    x = 8

    expect = 2

    actual = Solution().mySqrt(x)

    assert actual == expect


def test_9():

    x = 9

    expect = 3

    actual = Solution().mySqrt(x)

    assert actual == expect
