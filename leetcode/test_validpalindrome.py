###############################################################################
# Filename: test_validpalindrome.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_validpalindrome.py
#
###############################################################################

import re


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def isPalindrome(self, s: str) -> bool:
        """
        Determine whether the string is a palindrome.

        Notes:
          - Only consider alphanumeric characters
          - Ignore cases
          - The empty string is a valid palindrome

        Intuition:
          Move two pointers, one from left to right and the other from right
          to left, comparing the two characters for equality.

        Algorithm:
          Move the pointers until they pass each other, at which point, the
          string is a palindrome. Immediately stop when the two characters are
          different.

        """
        # Ignore cases by standardizing to lower case.
        s = s.lower()

        # Consider only alphanumeric characters by removing everything else.
        pattern = re.compile('[^a-zA-Z0-9]+')
        s = pattern.sub('', s)

        # Palindrome check.
        left = 0
        right = len(s) - 1
        while left <= right:
            if s[left] != s[right]:
                return False
            left += 1
            right -= 1
        return True


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():

    s = 'A man, a plan, a canal: Panama'

    print(Solution().isPalindrome(s))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    s = 'A man, a plan, a canal: Panama'

    expect = True

    actual = Solution().isPalindrome(s)

    assert actual == expect


def test_example_2():

    s = 'race a car'

    expect = False

    actual = Solution().isPalindrome(s)

    assert actual == expect
