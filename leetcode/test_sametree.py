###############################################################################
# Filename: test_sametree.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_sametree.py
#
###############################################################################

from typing import Any, List, Union


###############################################################################
# Solution
###############################################################################

class TreeNode:
    """
    Definition for a binary tree node.

    """
    def __init__(self, x: Any):
        self.val = x
        self.left = None  # type: Union[TreeNode,None]
        self.right = None  # type: Union[TreeNode,None]


class Solution:
    def isSameTree(self,
                   p: TreeNode, q: TreeNode) -> bool:

        return self._isSameRecursive(p, q)

    def _isSameRecursive(self,
                         p: Union[TreeNode, None],
                         q: Union[TreeNode, None]) -> bool:

        # Both are None.
        if p is None and q is None:
            return True

        # Neither is None.
        elif p is not None and q is not None:

            # Node.
            if p.val != q.val:
                return False

            # Left sub-tree.
            if not self._isSameRecursive(p.left, q.left):
                return False

            # Right sub-tree.
            if not self._isSameRecursive(p.right, q.right):
                return False

            return True

        # One is None.
        else:
            return False


###############################################################################
# Test functions
###############################################################################


def binaryTreeFromArray(array: List[Any]) -> Union[TreeNode, None]:
    """
    Build binary tree from array of values.

    Returns:
      Root node of tree.

    """
    return insertBreadthFirst(array, 0, len(array))


def insertBreadthFirst(array: List[Any],
                       i: int, n: int) -> Union[TreeNode, None]:
    """
    Recursive function to insert nodes in level order.

    """

    # Base case for recursion.
    if i >= n:
        return None

    root = TreeNode(array[i])

    # Insert left sub-tree.
    root.left = insertBreadthFirst(array, 2 * i + 1, n)

    # Insert right sub-tree.
    root.right = insertBreadthFirst(array, 2 * i + 2, n)

    return root


def arrayFromBinaryTree(root: Union[TreeNode, None]) -> List[Any]:
    # Initialize return array.
    array: List[Any] = []

    # Initialize queue.
    queue: List[TreeNode] = []

    # Enqueue root.
    if root is not None:
        queue.append(root)

    # Add rest of node values.
    while len(queue) > 0:

        # Pop node at front of queue and add its value to array.
        node = queue.pop(0)
        array.append(node.val)

        # Enqueue left child of node.
        if node.left is not None:
            queue.append(node.left)

        # Enqueue right child of node.
        if node.right is not None:
            queue.append(node.right)

    return array


###############################################################################
# Driver
###############################################################################

def main():
    print(arrayFromBinaryTree(binaryTreeFromArray([1, 2, 3, 4, 5, 6])))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_simple():

    p = binaryTreeFromArray([1, 2, 3])
    q = binaryTreeFromArray([1, 2, 3])

    expect = True

    actual = Solution().isSameTree(p, q)

    assert actual == expect
