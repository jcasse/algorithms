###############################################################################
# Filename: test_regular_expression_matching.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_regular_expression_matching.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def isMatch(self, s: str, p: str) -> bool:
        """
        Determine whether pattern p matches string s.

        Notes:
            - pattern p can contain . and *
            - string s and pattern p can be empty
            - string s and pattern p contain only a-z (plus . and * for p)

        Example:
            Input:
                s = "aab"
                p = "c*a*b"
            Output: true
            Explanation: c can appear 0 times, a can appear 1 time.
                         Therefore, p matches "aab".

        Algorithm:
            Scan the pattern p, character by character combining each character
            with associated modifier. For each character (and modifier), match
            next character(s) on the string s. Return False if a match cannot
            be made. If both s and p match completely, return True.

        """
        si = 0
        pi = 0
        while True:

            # Stop if no more s or p.
            if si == len(s) or pi == len(p):
                break

            # Get next pattern character.
            char = p[pi]
            pi += 1
            star = False
            if pi < len(p) and p[pi] == '*':
                star = True
                pi += 1

            # Next char is needed to stop .* from matching the entire string.
            next_char = None
            if pi < len(p) - 1:
                next_char = p[pi + 1]

            # Find match in s.
            # Note:
            # Potential problem if, for example,  p = 'a*a' and s = 'aaa'.
            # We should use a* to match exactly 2 as.
            if char == '.':
                if star:
                    while si != len(s):
                        if not next_char or s[si] != next_char:
                            si += 1
                        else:
                            break
                else:
                    si += 1
            else:
                if star:
                    while si != len(s) and s[si] == char:
                        si += 1
                else:
                    # Checkpoint #1: Check whether there is no match.
                    if s[si] != char:
                        return False
                    si += 1

        # Checkpoint #2: Check whether there is a complete match.
        if si == len(s) and pi == len(p):
            return True
        else:
            return False


# def get_next_char(p, i):
#     """
#     Get the next character to match and how many to match.

#     Also, updates the index of p.

#     Note:
#         Assumes i < len(p).

#     Returns:
#         (next_char, minimum, maximum, stop_char, next_p_index)

#     """
#     minimum = 1
#     maximum = 1
#     next_char = p[i]
#     i += 1
#     if i < len(p):
#         if p[i] == '*':
#             minimum = 0
#             maximum = None
#         else:
#             minimum = 1
#             maximum = 1
#     while i < len(i):
#         if p[i] == '*':
#             minimum = 0
#             maximum = None
#             i += 1
#         else:
#             if p[i] == next_char:
#                 minimum = minimum + 1
#             else:
#                 return next_char, minimum, maximum, p[i], i + 1
                

# def next_pair(p, i):
#     next_char = p[i]
#     i += 1
#     if i == len(p):
#         return next_char, None, i
#     else:
#         if p[i] == '*':
#             return next_char, True, i + 1
#         else:
#             if p[i] == next_char:
                
###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():

    s = 'aa'
    p = 'a*'

    print('s = "{}"'.format(s))
    print('p = "{}"'.format(p))
    print(Solution().isMatch(s, p))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################


def test_empty_s():

    s = ''
    p = 'a'

    expect = False

    actual = Solution().isMatch(s, p)

    assert actual == expect


def test_empty_p():

    s = 'a'
    p = ''

    expect = False

    actual = Solution().isMatch(s, p)

    assert actual == expect


def test_empty_both():

    s = ''
    p = ''

    expect = True

    actual = Solution().isMatch(s, p)

    assert actual == expect


def test_example_1():

    s = 'aa'
    p = 'a'

    expect = False

    actual = Solution().isMatch(s, p)

    assert actual == expect


def test_example_2():

    s = 'aa'
    p = 'a*'

    expect = True

    actual = Solution().isMatch(s, p)

    assert actual == expect


def test_example_3():

    s = 'ab'
    p = '.*'

    expect = True

    actual = Solution().isMatch(s, p)

    assert actual == expect


def test_example_4():

    s = 'aab'
    p = 'c*a*b'

    expect = True

    actual = Solution().isMatch(s, p)

    assert actual == expect


def test_example_5():

    s = 'mississippi'
    p = 'mis*is*p*'

    expect = False

    actual = Solution().isMatch(s, p)

    assert actual == expect


def dont_test_example_tricky():

    s = 'aaa'
    p = 'a*a'

    expect = True

    actual = Solution().isMatch(s, p)

    assert actual == expect
