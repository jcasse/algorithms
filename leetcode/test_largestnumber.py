###############################################################################
# Filename: test_twosum.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_twosum.py
#
###############################################################################

from typing import List


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def largestNumber(self, nums: List[int]) -> str:
        """
        Arrange nums such that they form the largest number.

        Notes:
          - Integers are non-negative

        Algorithm:
          Sort the array in descending lexicographical order. For this to work,
          all numbers must be the same length. Shorter numbers are expanded.
          This is the tricky part: We expand the shorter numbers by appending
          the larger of the most- and least-significant digits. This is what
          will determine the correct ordering of numbers sharing the most
          significant digit.
          Once sorted, concatenate the numbers, removing the added digits.

        Time complexity:
          O((n log n) * num_chars)

        Solution by Leetcode:

          Convert numbers to strings. Sort by decreasing lexicographical order
          using a custom comparator the checks both placements of the two
          strings being compared. This takes care of the case where two
          string start with the same number.

          class LargerNumKey(str):
            def __lt__(x, y):
              return x+y > y+x

          class Solution:
            def largestNumber(self, nums):
              largest_num = ''.join(sorted(map(str, nums), key=LargerNumKey))
              return '0' if largest_num[0] == '0' else largest_num

        """
        # Convert numbers to strings.
        # Time complexity: O(n * max_num_chars)
        nums = [str(x) for x in nums]

        # Convert to tuples: (number, number_characters)
        # Time complexity: O(n)
        nums = [(x, len(x)) for x in nums]

        # Compute maximum number of digits.
        # Time complexity: O(n)
        maxlen = 0
        for (x, _) in nums:
            if len(x) > maxlen:
                maxlen = len(x)

        # ** Make numbers the same length by repeating the larger of the **
        # ** most-significant digit and the least-significant digit at   **
        # ** the end of the string.                                      **
        # Time complexity: O(n)
        nums = [(x + (x[0] if x[0] > x[-1] else x[-1]) * (maxlen - l), l)
                for (x, l) in nums]

        # Sort by decreasing lexicographical order.
        # Time complexity: O((n log n) * num_chars)
        nums = sorted(nums, reverse=True)

        # Assemble output string by concatenating characters according to
        # their original length.
        # Time complexity: O(total number of characters)
        ret = ''
        for (x, l) in nums:
            for i in range(l):
                ret += x[i]

        # Remove possible leading zeros,
        # but do not delete the last one if the value is 0.
        # Time complexity: O(n)
        while len(ret) > 1 and ret[0] == '0':
            ret = ret[1:]

        return ret


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    nums = [10, 2]
    print(nums)
    print(Solution().largestNumber(nums))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example_1():

    nums = [10, 2]

    expect = "210"

    actual = Solution().largestNumber(nums)

    assert actual == expect


def test_example_2():

    nums = [3, 30, 34, 5, 9]

    expect = "9534330"

    actual = Solution().largestNumber(nums)

    assert actual == expect


def test_other():

    nums = [121, 12]

    expect = '12121'

    actual = Solution().largestNumber(nums)

    assert actual == expect


def test_zero():

    nums = [0, 0]

    expect = '0'

    actual = Solution().largestNumber(nums)

    assert actual == expect


def test_more_zeros():

    nums = [0, 0, 0, 0]

    expect = '0'

    actual = Solution().largestNumber(nums)

    assert actual == expect


def test_long():

    nums = [824, 938, 1399, 5607, 6973, 5703, 9609, 4398, 8247]

    expect = '9609938824824769735703560743981399'

    actual = Solution().largestNumber(nums)

    assert actual == expect
