###############################################################################
# Filename: test_3sum.py
# Environment: Python3
# Testing: Type on the command-line:
#          pytest --durations=0 -vv test_3sum.py
#
###############################################################################

import json
import collections
from typing import List

import pytest


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        """
        Find elements in nums whose sum equals zero: a + b + c = 0.

        Notes:
          - Find all solutions
          - Solutions must be unique in terms of the elements it contains,
            that is, no two solutions can have the same combination of three
            -array elements- numbers?

        Example:
            Given array nums = [-1, 0, 1, 2, -1, -4],

            A solution set is:
            [
                [-1, 0, 1],
                [-1, -1, 2]
            ]

        Algorithm:
          1. Store all distinct pairs sums into hash table
             mapping sum -> set of solutions -indices-
          2. For each element, if -sum exists in hash table, add index
             to set of indices
          3. Add all trios to final solution list

        Args:
          nums (list): Integers.

        Returns:
          List[List[int]]: List of unique solutions.

        """
        return self.unique_list_indexes_version(nums)

    def unique_list_indexes_version(self, nums):
        # Construct hash table of unique pair sums: sum -> [(i, j), ...]
        # Upper right triangle of symmetric matrix of pairs.
        n = len(nums)
        hashtable = {}
        for i in range(n):
            for j in range(i + 1, n):

                # Compute sum of pair.
                s = nums[i] + nums[j]

                # First-time add sum key to hash table.
                if s not in hashtable:
                    hashtable[s] = []

                # Add unique pair to set of pairs that sum to the same value.
                # Tuple because we can then ask for membership of third item.
                hashtable[s].append((i, j))

        # Find third item.
        res = set()
        for i, x in enumerate(nums):

            pairs = hashtable.get(-x)

            # Complement not found as a key in the hash table.
            if pairs is None:
                continue

            # Add to pair.
            for pair in pairs:

                # No duplicates.
                if i not in pair:
                    res.add(tuple(sorted([i, pair[0], pair[1]])))

        # Convert from indexes to values, removing duplicate triplets.
        final = set()
        for t in res:
            final.add(tuple(sorted([nums[t[0]], nums[t[1]], nums[t[2]]])))

        return list(final)

    def unique_values_version(self, nums):
        # Construct hash table of unique pair sums: sum -> [(i, j), ...]
        # Upper right triangle of symmetric matrix of pairs.
        n = len(nums)
        hashtable = collections.defaultdict(set)
        for i in range(n):
            for j in range(i + 1, n):

                # Compute sum of pair.
                s = nums[i] + nums[j]

                # Add unique pair to set of pairs that sum to the same value.
                # Tuple because we can then ask for membership of third item.
                hashtable[s].add(tuple(sorted([nums[i], nums[j]])))

        # Find third item.
        res = set()
        for i, x in enumerate(nums):

            pairs = hashtable.get(-x)

            # Complement not found as a key in the hash table.
            if pairs is None:
                continue

            # Add to pair.
            for pair in pairs:

                # No duplicates.
                if x not in pair:
                    res.add(tuple(sorted([x, pair[0], pair[1]])))

        ret = []
        for triple in res:
            ret.append(triple)

        return ret


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    nums = [-1, 0, 1, 2, -1, -4]
    result = Solution().threeSum(nums)
    print(result)


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_simple():

    nums = [-1, 0, 1, 2, -1, -4]
    expect = [(-1, -1, 2), (-1, 0, 1)]

    actual = Solution().threeSum(nums)

    print(expect)
    print(actual)

    assert actual == expect


def test_long():

    nums = [-12, -1, 4, -14, 0, 10, 7, -7, -6, 9, 6, -2, 7, 13, 9, -1, 4, 12,
            9, 4, 14, 0, -4, 0, 0, 10, 2, -7, 7, -4, -11, 10, 2, 8, 4, -12,
            -4, -12, -4, -3, 12, 9, 11, 4, -1, -3, 10, -12, -6, -4, -1, -14,
            3, 2, -7, -11, -3, 10, -11, -10, 13, -15, 7, 0, 0, -4, -5, 11, 0,
            -2, -14, -11, -8, 12, 1, -1, -14, -12, -6, -5, 0, 9, -4, -12, -4,
            4, 14, 9, -9, 10, 14, -11, 10, 6, -3, -4, 10, -1, 14, -13, 13, 7,
            -9, 12, 4, -1, -4, 5, 3, 6, 8, 10, 0, 11, 13, 11, -7, 5, -3, -1,
            0, -4, -4, -4, 10, 0]
    expect = [(-8, -2, 10), (-7, 2, 5), (-13, 6, 7), (-9, -1, 10), (-2, -2, 4), (-7, -6, 13), (-3, -2, 5), (-7, 3, 4), (-12, 4, 8), (-14, 2, 12), (-12, 3, 9), (-13, 3, 10), (-5, -1, 6), (-12, 5, 7), (-11, -2, 13), (-10, 0, 10), (-9, 2, 7), (-8, 0, 8), (-7, -3, 10), (-13, 1, 12), (-15, 7, 8), (-11, 1, 10), (-5, -4, 9), (-13, -1, 14), (-8, 1, 7), (-6, 3, 3), (-8, -3, 11), (-14, 5, 9), (-3, 1, 2), (-4, -1, 5), (-12, 2, 10), (-12, -1, 13), (-10, 5, 5), (-7, -2, 9), (-10, -2, 12), (-11, 2, 9), (-9, 4, 5), (-13, 0, 13), (-7, -5, 12), (-5, -3, 8), (-4, 2, 2), (-9, 3, 6), (-6, 2, 4), (-5, 0, 5), (-8, -4, 12), (-2, 0, 2), (-14, 4, 10), (-5, 1, 4), (-2, -1, 3), (-6, -5, 11), (-9, 0, 9), (-14, 1, 13), (-12, 1, 11), (-10, 3, 7), (-8, -1, 9), (-10, 4, 6), (-11, 3, 8), (-6, -2, 8), (-4, 1, 3), (-3, -1, 4), (-9, -2, 11), (-13, 4, 9), (-8, -5, 13), (-15, 1, 14), (-11, 4, 7), (-11, -1, 12), (-8, 4, 4), (-15, 4, 11), (-9, 1, 8), (-5, -2, 7), (-6, -6, 12), (-7, 0, 7), (-14, 0, 14), (-12, 0, 12), (-10, 2, 8), (-10, -3, 13), (-5, -5, 10), (-10, -1, 11), (-4, 0, 4), (-13, 5, 8), (-15, 2, 13), (-6, 1, 5), (-14, 7, 7), (-8, -6, 14), (-4, -2, 6), (-6, -3, 9), (-7, -1, 8), (-6, -1, 7), (-11, 5, 6), (-8, 3, 5), (-9, -3, 12), (-15, 5, 10), (-12, -2, 14), (-1, 0, 1), (-7, 1, 6), (-10, -4, 14), (-13, 2, 11), (-3, -3, 6), (-14, 3, 11), (-7, -7, 14), (-4, -4, 8), (-12, 6, 6), (-7, -4, 11), (0, 0, 0), (-6, 0, 6), (-10, 1, 9), (-14, 6, 8), (-11, 0, 11), (-1, -1, 2), (-5, 2, 3), (-4, -3, 7), (-11, -3, 14), (-6, -4, 10), (-15, 3, 12), (-9, -4, 13), (-8, 2, 6), (-15, 6, 9), (-3, 0, 3), (-9, -5, 14)]

    actual = Solution().threeSum(nums)

    print(expect)
    print(actual)

    assert actual == expect


@pytest.mark.xfail(run=False, reason='takes too long')
def test_311_of_313():

    with open('data/3sum/test_311_of_313.txt') as fd:
        nums = json.loads(fd.read())

    actual = Solution().threeSum(nums)

    assert False
