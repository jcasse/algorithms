###############################################################################
# Filename: test_longestcommonprefix.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_longestcommonprefix.py
#
###############################################################################

import sys


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        # Empty list.
        if not strs:
            return ''

        # Find min length.
        minlen = sys.maxsize
        for string in strs:
            currlen = len(string)
            if minlen > currlen:
                minlen = currlen

        # Find longest common prefix.
        ret = []
        for pos in range(minlen):
            char = self._charInCommonAtPos(pos, strs)
            if char is None:
                break
            else:
                ret.append(char)

        # Return answer.
        return ''.join(ret)

    def _charInCommonAtPos(self, pos, strs):
        """
        Return common character at pos.

        If not the same char in all strings, returns None.

        Note:
          Assumes pos is within len of strings.

        Args:
          pos (int): Index into string.
          strs (list): List of strings.

        Returns:
          (str): Common character or None.

        """
        char = None
        for string in strs:
            if char is None:
                char = string[pos]
            else:
                if char != string[pos]:
                    return None
        return char


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    strs = ['flower', 'flow', 'flight']
    print(strs)
    print(Solution().longestCommonPrefix(strs))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_simple():

    strs = ['flower', 'flow', 'flight']
    expect = 'fl'

    actual = Solution().longestCommonPrefix(strs)

    assert actual == expect


def test_no_common_prefix():

    strs = ['dog', 'racecar', 'car']
    expect = ''

    actual = Solution().longestCommonPrefix(strs)

    assert actual == expect


def test_empty_list():

    strs = []
    expect = ''

    actual = Solution().longestCommonPrefix(strs)

    assert actual == expect
