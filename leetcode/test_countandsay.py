###############################################################################
# Filename: test_countandsay.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_countandsay.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        nums = '1'
        for i in range(1, n):
            nums = self._nextSequence(nums)
        return nums

    def _nextSequence(self, nums):
        ret = ''
        index = 0
        while index < len(nums):
            count, number, index = self._getCount(nums, index)
            ret = ret + str(count) + number
        return ret

    def _getCount(self, nums, index):
        count = 0
        number = nums[index]
        while index < len(nums):
            if nums[index] != number:
                break
            count = count + 1
            index = index + 1
        return count, number, index


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    print(Solution().countAndSay(n=5))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_1():

    n = 1

    expect = '1'

    actual = Solution().countAndSay(n)

    assert actual == expect


def test_2():

    n = 2

    expect = '11'

    actual = Solution().countAndSay(n)

    assert actual == expect


def test_3():

    n = 3

    expect = '21'

    actual = Solution().countAndSay(n)

    assert actual == expect


def test_4():

    n = 4

    expect = '1211'

    actual = Solution().countAndSay(n)

    assert actual == expect


def test_5():

    n = 5

    expect = '111221'

    actual = Solution().countAndSay(n)

    assert actual == expect
