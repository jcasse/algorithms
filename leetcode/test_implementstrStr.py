###############################################################################
# Filename: test_implementstrstr.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_implementstrstr.py
#
###############################################################################


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def strStr(self, haystack, needle):
        """
        Find index of first occurrence of needle in haystack.

        :type haystack: str
        :type needle: str
        :rtype: int
        """
        # Empty needle.
        if not needle:
            return 0

        # Find needle in haystack.
        n = len(haystack)
        m = len(needle)
        index = -1
        while True:

            # Find next start of possible needle match.
            while index < n:
                index = index + 1
                if index < n and haystack[index] == needle[0]:
                    break

            # Early termination. Needle is larger than remainder of haystack.
            if m > n - index:
                index = n

            # Needle not found condition.
            if index == n:
                break

            # Check for complete needle string.
            j = 0
            while j < m and index + j < n:
                if needle[j] != haystack[index + j]:
                    break
                j = j + 1

            # Needle found condition.
            if j == m:
                break

        return -1 if index == n else index


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    haystack = 'hello'
    needle = 'll'
    print(Solution().strStr(haystack, needle))


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_simple_present():

    haystack = 'hello'
    needle = 'll'

    expect = 2

    actual = Solution().strStr(haystack, needle)

    assert actual == expect


def test_simple_not_present():

    haystack = 'aaaaa'
    needle = 'bba'

    expect = -1

    actual = Solution().strStr(haystack, needle)

    assert actual == expect


def test_empty_needle():

    haystack = 'aaaa'
    needle = ''

    expect = 0

    actual = Solution().strStr(haystack, needle)

    assert actual == expect


def test_large():

    with open('data/haystack.txt', 'r') as f:
        haystack = f.read()
    with open('data/needle.txt', 'r') as f:
        needle = f.read()

    expect = -1

    actual = Solution().strStr(haystack, needle)

    assert actual == expect
