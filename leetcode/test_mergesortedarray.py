###############################################################################
# Filename: test_mergesortedarray.py
# Environment: Python3
# Code Style: PEP8
# Testing: Type on the command-line: pytest test_mergesortedarray.py
#
###############################################################################

from typing import List


###############################################################################
# Solution
###############################################################################

class Solution(object):
    def merge(self,
              nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Modify nums1 in-place to include nums2.

        Notes:
          - nums1 has enough space for nums2 elements

        Algorithm:
          Since nums1 has enough space at the end to add the elements of nums2,
          add the elements backwards, from largest to smallest to the end of
          the nums1 array. Need to maintain three indexes, one to the next
          largest value in each array and one to the next value in the
          resulting array.

        """
        # Initialize indexes.
        i: int = m - 1
        j: int = n - 1
        k: int = m + n - 1

        # Merge nums2 into nums1.
        while k >= 0:

            # Elements remaining in both arrays.
            if i >= 0 and j >= 0:
                if nums1[i] > nums2[j]:
                    nums1[k] = nums1[i]
                    i -= 1
                else:
                    nums1[k] = nums2[j]
                    j -= 1

            # Copy the remaining elements from nums2.
            elif j >= 0:
                nums1[k] = nums2[j]
                j -= 1

            # The remaining elements are already in nums1.
            else:
                break

            # Advance resulting array index.
            k -= 1


###############################################################################
# Test functions
###############################################################################

###############################################################################
# Driver
###############################################################################

def main():
    m = 3
    nums1 = [1, 2, 3, 0, 0, 0]
    n = 3
    nums2 = [2, 5, 6]

    print(nums1)
    print(nums2)

    Solution().merge(nums1, m, nums2, n)

    print(nums1)


if __name__ == '__main__':
    main()


###############################################################################
# Unit Tests
###############################################################################

def test_example():
    m = 3
    nums1 = [1, 2, 3, 0, 0, 0]
    n = 3
    nums2 = [2, 5, 6]

    expect = [1, 2, 2, 3, 5, 6]

    Solution().merge(nums1, m, nums2, n)

    assert nums1 == expect


def test_nums1_empty():
    m = 0
    nums1 = [0, 0, 0]
    n = 3
    nums2 = [2, 5, 6]

    expect = [2, 5, 6]

    Solution().merge(nums1, m, nums2, n)

    assert nums1 == expect


def test_nums2_empty():
    m = 3
    nums1 = [3, 5, 9]
    n = 0
    nums2 = [0, 0, 0]

    expect = [3, 5, 9]

    Solution().merge(nums1, m, nums2, n)

    assert nums1 == expect


def test_empty():
    m = 0
    nums1 = []
    n = 0
    nums2 = []

    expect = []

    Solution().merge(nums1, m, nums2, n)

    assert nums1 == expect
