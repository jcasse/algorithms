###############################################################################
# File name: radix_sort.py
# https://www.youtube.com/
#         watch?v=nu4gDuFabIM&list=PLqM7alHXFySHrGIxeBOo4-mKO4H8j2knW&index=8
#
###############################################################################

from counting_sort import countingSort

from collections import OrderedDict


def radixSort(A: list, Z: list, W: int) -> None:
    """
    Sort array in-place.

    Notes:
      - Works only when all possible values are known and finite
      - Needs works

    Args:
      A: Values to sort.
      Z: Sorted alphabet.
      W: Maximum width of values.

    Intuition:
      Sorts in lexicographical order, one character at a time, starting from
      the least significant character. At each position, sorting is done
      by counting sort.

    Algorithm:
      For each position, sort according to that position using counting sort.

    Time complexity:
      O(k + n + w): Where k is the size of the alphabet, n is the size of the
                    input array and w is the width of the words (number of
                    characters).

    """
    for i in range(W):
        countingSort(A, Z)


def main():

    A = [1, 4, 1, 2, 7, 5, 2]
    print(A)

    radixSort(A, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 1)

    print(A)


if __name__ == "__main__":
    main()


def test_empty():
    A = []

    expect = []

    countingSort(A, [])

    actual = A

    assert actual == expect


def test_single():
    A = [5]

    expect = [5]

    countingSort(A, [5])

    actual = A

    assert actual == expect


def test_general():
    A = ['one', 'four', 'one', 'two', 'seven', 'five', 'two']

    expect = ['one', 'one', 'two', 'two', 'four', 'five', 'seven']

    countingSort(A, ['one', 'two', 'four', 'five', 'seven'])

    actual = A

    assert actual == expect
