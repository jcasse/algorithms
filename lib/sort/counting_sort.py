###############################################################################
# File name: counting_sort.py
# https://www.youtube.com/watch?v=7zuGmKfUt7s
#
###############################################################################

from collections import OrderedDict


def countingSort(A: list, Z: list) -> None:
    """
    Sort array in-place.

    Notes:
      - Works only when all possible values are known and finite

    Args:
      A: Values to sort.
      Z: Sorted alphabet.

    Intuition:
      By laying out the entire alphabet in order and counting the number of
      occurrences of each value in the original array, we can compute the
      indices in the sorted array for each value because we know that a certain
      value comes after all the other values previous to it in the order.

    Algorithm:
      1. Create array of count of occurrences of each value
         This is done for all possible values, in order of value.
      2. Convert counts to indices into sorted array
         This is done by incrementally adding counts from left to right.
      3. Place values in sorted array according to their indices
         This is done by iterating over the original array and placing that
         value according to its index, decrementing the index each time after
         placement for next possible occurrence.

    Time complexity:
      O(k + n): Where k is the size of the alphabet and n is the size of the
                input array.

    """
    # Create new dictionary of counts.
    # Time complexity: O(k + n)
    counts = OrderedDict()
    for x in Z:
        counts[x] = 0
    for x in A:
        counts[x] += 1

    # Convert counts to indices.
    # Time complexity: O(k)
    for i, x in enumerate(counts):
        if i == 0:
            cummulative = counts[Z[0]]
        else:
            cummulative += counts[x]
            counts[x] = cummulative

    # Create new sorted array.
    # Time complexity: O(n)
    S = [None] * len(A)
    for x in A:
        counts[x] -= 1
        index = counts[x]
        S[index] = x

    # Copy to original array.
    # Time complexity: O(n)
    for i in range(len(A)):
        A[i] = S[i]


def main():

    A = [1, 4, 1, 2, 7, 5, 2]
    print(A)

    countingSort(A, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    print(A)


if __name__ == "__main__":
    main()


def test_empty():
    A = []

    expect = []

    countingSort(A, [])

    actual = A

    assert actual == expect


def test_single():
    A = [5]

    expect = [5]

    countingSort(A, [5])

    actual = A

    assert actual == expect


def test_general():
    A = ['one', 'four', 'one', 'two', 'seven', 'five', 'two']

    expect = ['one', 'one', 'two', 'two', 'four', 'five', 'seven']

    countingSort(A, ['one', 'two', 'four', 'five', 'seven'])

    actual = A

    assert actual == expect
