###############################################################################
# File name: quicksort.py
# https://www.youtube.com/watch?v=SLauY6PpjW4
#
###############################################################################


def quicksort(A: list) -> None:
    """
    Sort array in-place.

    Notes:
      - Recursive

    Intuition:
      Divide and conquer algorithm. Recursively, select a pivot and put all
      elements smaller than pivot to the left of it and all elements bigger
      to the right. At this point, the pivot is at the right place.

    Time complexity:
      O(n log n): On average.

    """
    _quicksort(A, 0, len(A) - 1)


def _quicksort(A, left, right):
    if left < right:
        pivotFinalLocation = _partition(A, left, right)
        _quicksort(A, left, pivotFinalLocation - 1)
        _quicksort(A, pivotFinalLocation + 1, right)


def _partition(A, left, right):

    # Pick pivot.
    pivot = _pivot(A, left, right)

    # Partition.
    while True:
        while left < right and A[left] <= A[pivot]:
            left += 1
        while left < right and A[right] > A[pivot]:
            right -= 1
        if left >= right:
            break
        _swap(A, left, right)

    # Place pivot in its final, sorted location.
    _swap(A, left, pivot)

    # Return final location of pivot that will be used as the partition point
    # for subsequent calls to quicksort on sub arrays.
    return left


def _pivot(A, left, right, strategy='original'):
    if strategy == 'median of three':
        return (left + right) // 2
    else:
        return right


def _swap(A, i, j):
    temp = A[i]
    A[i] = A[j]
    A[j] = temp


def main():

    A = [15, 3, 9, 8, 5, 2, 7, 1, 6]
    print(A)

    quicksort(A)
    print()
    print(A)


if __name__ == '__main__':
    main()


def test_found():

    graph = SouthernGermany.graph
    start = 'Frankfurt'
    goal = 'Erfurt'

    expect = 'Erfurt'

    actual = bfs(graph, start, goal)

    assert actual == expect


def test_not_found():

    graph = SouthernGermany.graph
    start = 'Frankfurt'
    goal = 'Berlin'

    expect = None

    actual = bfs(graph, start, goal)

    assert actual == expect
