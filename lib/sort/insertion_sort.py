from typing import List


def insertion_sort(array: List[int]):
    for i in range(len(array)):
        sort_up_to_i(array, i)


def sort_up_to_i(array, i):
    while i > 0 and array[i - 1] > array[i]:
        temp = array[i]
        array[i] = array[i - 1]
        array[i - 1] = temp
        i -= 1


def test_empty():
    array = []

    expect = []

    insertion_sort(array)

    actual = array

    assert actual == expect


def test_singleton():
    array = [4]

    expect = [4]

    insertion_sort(array)

    actual = array

    assert actual == expect


def test_two_sorted():
    array = [1, 4]

    expect = [1, 4]

    insertion_sort(array)

    actual = array

    assert actual == expect


def test_two_unsorted():
    array = [5, 4]

    expect = [4, 5]

    insertion_sort(array)

    actual = array

    assert actual == expect


def test_best_case():
    array = [1, 2, 3, 4, 5]

    expect = [1, 2, 3, 4, 5]

    insertion_sort(array)

    actual = array

    assert actual == expect


def test_worst_case():
    array = [5, 4, 3, 2, 1]

    expect = [1, 2, 3, 4, 5]

    insertion_sort(array)

    actual = array

    assert actual == expect
