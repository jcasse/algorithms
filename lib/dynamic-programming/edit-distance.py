def edit_distance(string, other):
    """
    From Algorithms by Dasgupta, Papdimitriou, Vazirani.

    """

    # Initialize table.
    E = [[0 for i in range(len(other) + 1)] for i in range(len(string) + 1)]
    for i in range(len(string) + 1):
        E[i][0] = i
    for j in range(len(other) + 1):
        E[0][j] = j

    # Define difference function.
    # It is 1 if need to replace on character for the other.
    def d(i, j):
        return 0 if string[i-1] == other[j-1] else 1

    # Incrementally, solve sub problems in order.
    for i in range(1, len(string) + 1):
        for j in range(1, len(other) + 1):
            E[i][j] = min(E[i-1][j] + 1, E[i][j-1] + 1, E[i-1][j-1] + d(i, j))

    return E[len(string)][len(other)]


class Element(object):
    def __init__(self, i=-1, j=-1, distance=0):
        self.i = i
        self.j = j
        self.d = distance

    def __repr__(self):
        return '({} {} {})'.format(self.i, self.j, self.d)


def edit_path(string, other):
    """
    From Algorithms by Dasgupta, Papdimitriou, Vazirani.

    """

    # Initialize table.
    E = [[Element() for i in range(len(other) + 1)] for i in range(len(string) + 1)]
    for i in range(len(string) + 1):
        E[i][0].i = i
        E[i][0].j = 0
        E[i][0].d = i
    for j in range(len(other) + 1):
        E[0][j].i = 0
        E[0][j].j = j
        E[0][j].d = j

    print('-' * 80)
    for row in E:
        for col in row:
            print(col, end='')
        print()
    print('-' * 80)

    # Define difference function.
    # It is 1 if need to replace on character for the other.
    def d(i, j):
        return 0 if string[i-1] == other[j-1] else 1

    def minimum(i, j):
        # Algorithms book, bottom of page 177 says a move down is a deletion,
        # a move right is an insertion and a diagonal move is either a
        # substitution or match.
        # This makes sense with respect to the first string, which its
        # characters are on the left axis of the table, i.e., each character
        # is the label for a row.
        deletion = E[i-1][j].d + 1
        insertion = E[i][j-1].d + 1
        substitution_or_match = E[i-1][j-1].d + d(i, j)
        if deletion < insertion:
            if deletion < substitution_or_match:
                return (i-1, j, deletion)
            else:
                return (i-1, j-1, substitution_or_match)
        else:
            if insertion < substitution_or_match:
                return (i, j-1, insertion)
            else:
                return (i-1, j-1, substitution_or_match)

    # Incrementally, solve sub problems in order.
    for i in range(1, len(string) + 1):
        for j in range(1, len(other) + 1):
            prev_i, prev_j, dist = minimum(i, j)
            E[i][j].i = prev_i
            E[i][j].j = prev_j
            E[i][j].d = dist

    print('-' * 80)
    for row in E:
        for col in row:
            print(col, end='')
        print()
    print('-' * 80)

    return E


def get_edit_distance(E):
    return E[-1][-1].d


def get_path(path, E, i, j):
    if i == 0 and j == 0:
        return
    path.append((i, j))
    if i > 0 and j > 0:
        prev_i = E[i][j].i
        prev_j = E[i][j].j
        return get_path(path, E, prev_i, prev_j)
    elif i > 0:
        return get_path(path, E, i-1, j)
    elif j > 0:
        return get_path(path, E, i, j-1)


def main():
    string = 'EXPONENTIAL'
    other = "POLYNOMIAL"
    print('Edit distance = {}'.format(edit_distance(string, other)))

    E = edit_path(string, other)
    print('Edit distance = {}'.format(get_edit_distance(E)))

    path = []
    get_path(path, E, len(string), len(other))

    prev_i = 0
    for index in reversed(path):
        i, _ = index
        if i > prev_i:
            print(string[i - 1], end='')
        else:
            print('-', end='')
        prev_i = i
    print()

    prev_j = 0
    for index in reversed(path):
        _, j = index
        if j > prev_j:
            print(other[j - 1], end='')
        else:
            print('-', end='')
        prev_j = j
    print()


if __name__ == '__main__':
    main()
