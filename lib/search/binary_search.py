import pytest


def binary_search_recursive(target, sorted_array, left, right):
    if left > right:
        return None

    mid = (right + left) // 2

    if sorted_array[mid] == target:
        return mid
    elif sorted_array[mid] < target:
        return binary_search_recursive(target, sorted_array, mid+1, right)
    else:
        return binary_search_recursive(target, sorted_array, left, right+1)


def binary_search_iterative(target, sorted_array):
    left = 0
    right = len(sorted_array) - 1

    while left <= right:
        mid = (left + right) // 2
        if sorted_array[mid] < target:
            left = mid + 1
        elif sorted_array[mid] > target:
            right = mid - 1
        else:
            return mid

    return None


def parameters():
    return [
        pytest.param(
            8, [], None, id='empty'),
        pytest.param(
            8, [1, 2, 3, 4, 5, 6, 7], None, id='not found'),
        pytest.param(
            4, [1, 2, 3, 4, 5, 6, 7, 8], 3, id='even length'),
        pytest.param(
            4, [1, 2, 3, 4, 5, 6, 7], 3, id='odd length')
        ]


@pytest.mark.parametrize('target, sorted_array, expect', parameters())
def test_iterative(target, sorted_array, expect):
    actual = binary_search_iterative(target, sorted_array)
    assert actual == expect


@pytest.mark.parametrize('target, sorted_array, expect', parameters())
def test_recursive(target, sorted_array, expect):
    left = 0
    right = len(sorted_array) - 1
    actual = binary_search_recursive(target, sorted_array, left, right)
    assert actual == expect
