import sys
import argparse
import textwrap


def getCommandLineArgs():
    """
    Handles command-line arguments.

    :returns: (Namespace) processed command-line arguments

    """
    STRING = 'Some books are to be eaten, others to be swallowed'
    PATTERN = 'to'
    parser = argparse.ArgumentParser(
        description='String Search',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
        Naive string search.

        example out-of-the-box usage:
          python %(prog)s --pattern "me" --string "some string"
        '''))
    parser.add_argument('--pattern',
                        help='pattern to find in string',
                        default=PATTERN)
    parser.add_argument('--string',
                        help='string to search',
                        default=STRING)
    return parser.parse_args()


def string_search(pattern, string):

    ret = []
    for i in range(len(string)):
        end = match_sub_string(pattern, string, i)
        if end:
            ret.append((i, end))
    return ret


def match_sub_string(pattern, string, start):
    """
    Returns:
        end index

    """
    if len(pattern) - 1 + start > len(string):
        return None

    end = start
    for char in pattern:
        if string[end] != char:
            return None
        else:
            end += 1

    return end


def main(args):

    string = args.string
    pattern = args.pattern

    matches = string_search(pattern, string)

    for match in matches:
        print('"{}" at ({}, {})'.format(
            string[match[0]:match[1]], match[0], match[1]))

    if not matches:
        print('pattern "{}" not found.'.format(pattern))


if __name__ == '__main__':
    args = getCommandLineArgs()
    main(args)
