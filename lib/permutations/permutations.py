def get_word(objects, indices):
    word = []
    for index in indices:
        word.append(objects[index])
    return word


def main():

    objects = [0, 1]
    slots = 3

    indices = [0] * slots
    change = True
    while change:

        print(get_word(objects, indices))

        change = False
        for slot in reversed(range(slots)):
            # Reset slot index if reached end.
            if indices[slot] == len(objects) - 1:
                indices[slot] = 0
            # Increment slot index and exit.
            else:
                indices[slot] += 1
                change = True
                break


if __name__ == '__main__':
    main()
