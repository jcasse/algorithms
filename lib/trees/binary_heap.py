class Item(object):
    """
    Example object type for BinaryHeap.

    """
    def __init__(self, value, key):
        self.value = value
        self.key = key

    def __str__(self):
        return '{}'.format(self.value)

    def __lt__(self, other):
        """
        item1 < item2 calls item1.__lt__(item2)

        """
        return self.key < other.key

    def __eq__(self, other):
        """
        item1 == item2 calls item1.__eq__(item2)
        """
        return self.key == other.key

    def increaseKey(self, amount):
        self.key += amount

    def decreaseKey(self, amount):
        self.key -= amount


class BinaryHeap(object):
    """
    Implementation of the binary heap data structure.

    Properties of the binary heap:
        - structure property: complete binary tree (complete means that all
          the levels are filled, except possibly the last level is partially
          filled from left to right.
        - heap order property: for any node X, its parent is smaller than or
          equal (min-heap) or greater than or equal (max-heap).

    Basic operations:
        - insert: O(log n)
        - remove: O(log n)

    Because of the heap order property, the items in the binary heap must
    support the comparison operation.

    If decreasKey and increaseKey are to be used, the item type must also
    support such operations.

    """
    def __init__(self, items=None, reverse=False):
        """
        Initialize empty.

        Either an empty heap or a heap containing the items.

        Args:
            items: must be iterable
            invert: whether this is a maxHeap

        """
        self._reverse = reverse
        self._buildHeap(items)

    def isEmpty(self):
        return self._length == 0

    def insert(self, item):
        """
        """
        self._heapArray.append(item)
        self._length += 1
        self._percolateUp(self._length)

    def peek(self):
        return self._heapArray[1]

    def pop(self):
        """
        """
        if self.isEmpty():
            return None

        item = self._heapArray[1]

        self._heapArray[1] = self._heapArray[self._length]
        self._length -= 1
        self._percolateDown(1)

        return item

    def findItem(self, item):
        print(self._heapArray)
        for i in range(1, self._length + 1):
            if self._heapArray[i].value == item.value:
                return i
        return None

    def decreaseKey(self, amount, position=1):
        self._heapArray[position].decreaseKey(amount)
        self._percolateUp(position)

    def increaseKey(self, amount, position=1):
        self._heapArray[position].increaseKey(amount)
        self._percolateDown(position)

    def _buildHeap(self, items):
        if not items:
            self._length = 0
            self._heapArray = [None]
            return
        self._length = len(items)
        self._heapArray = [None for i in range(self._length + 1)]
        for i, item in enumerate(items):
            self._heapArray[i + 1] = item
        i = self._length // 2
        while i > 0:
            self._percolateDown(i)
            i -= 1

    def _percolateUp(self, hole):
        """
        Find the position (array index) for the item in heapArray[hole].

        Assumes heapArray[0] has item also as a termination condition if
        item happens to be the minimum (or maximum).

        """
        item = self._heapArray[hole]

        while hole // 2 > 0:
            parent = hole // 2
            # Percolate.
            if self._comp(item, self._heapArray[parent]):
                self._heapArray[hole] = self._heapArray[parent]
                hole = parent
            else:
                break

        self._heapArray[hole] = item

    def _percolateDown(self, hole):
        """
        Find position (array index) for item in heapArray[hole].

        """
        item = self._heapArray[hole]

        while hole * 2 <= self._length:
            child = hole * 2
            # Find smallest child.
            if child != self._length:
                if self._comp(
                        self._heapArray[child + 1], self._heapArray[child]):
                    child = child + 1
            # Percolate.
            if self._comp(self._heapArray[child], item):
                self._heapArray[hole] = self._heapArray[child]
                hole = child
            else:
                break

        self._heapArray[hole] = item

    def _comp(self, left, right):
        """
        """
        if self._reverse:
            return left > right
        else:
            return left < right


###############################################################################
# Unit Tests
###############################################################################

def test_insert_sorted():
    items = ['H', 'D', 'E', 'F', 'G', 'B', 'I', 'A', 'J', 'C']

    heap = BinaryHeap()
    for item in sorted(items):
        heap.insert(item)

    for item in sorted(items):
        assert heap.pop() == item


def test_insert_sorted_reverse():
    items = ['H', 'D', 'E', 'F', 'G', 'B', 'I', 'A', 'J', 'C']

    heap = BinaryHeap()
    for item in sorted(items, reverse=True):
        heap.insert(item)

    for item in sorted(items):
        assert heap.pop() == item


def test_insert():
    items = ['H', 'D', 'E', 'F', 'G', 'B', 'I', 'A', 'J', 'C']

    heap = BinaryHeap()
    for item in items:
        heap.insert(item)

    for item in sorted(items):
        assert heap.pop() == item


def test_insert_custom_item_type():
    items = [Item('D', 4), Item('E', 5), Item('F', 6), Item('G', 7),
             Item('B', 2), Item('A', 1), Item('H', 8), Item('C', 3)]

    heap = BinaryHeap()
    for item in items:
        heap.insert(item)

    for item in sorted(items):
        assert heap.pop() == item


def test_increase_key():
    items = [Item('D', 6), Item('E', 8), Item('F', 10), Item('G', 12),
             Item('B', 2), Item('A', 1), Item('H', 14), Item('C', 4)]

    heap = BinaryHeap()
    for item in items:
        heap.insert(item)

    # Make Item A come after B.
    heap.increaseKey(2)

    # B should be the root of the heap.
    assert heap.peek() == items[4]


def test_decrease_key():
    items = [Item('D', 6), Item('E', 8), Item('F', 10), Item('G', 12),
             Item('B', 2), Item('A', 1), Item('H', 14), Item('C', 4)]

    heap = BinaryHeap()
    for item in items:
        heap.insert(item)

    item = items[4]
    position = heap.findItem(item)
    # Make Item B come before A.
    heap.decreaseKey(2, position)

    # B should be the root of the heap.
    assert heap.peek() == items[4]


def test_insert_max():
    items = ['H', 'D', 'E', 'F', 'G', 'B', 'I', 'A', 'J', 'C']

    heap = BinaryHeap(reverse=True)
    for item in items:
        heap.insert(item)

    for item in sorted(items, reverse=True):
        assert heap.pop() == item


def test_build_heap_sorted():
    items = ['H', 'D', 'E', 'F', 'G', 'B', 'I', 'A', 'J', 'C']

    heap = BinaryHeap(sorted(items))

    for item in sorted(items):
        assert heap.pop() == item


def test_build_heap():
    items = ['H', 'D', 'E', 'F', 'G', 'B', 'I', 'A', 'J', 'C']

    heap = BinaryHeap(items)

    for item in sorted(items):
        assert heap.pop() == item
