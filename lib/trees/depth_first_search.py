###############################################################################
# File name: depth_first_search.py
# https://en.wikipedia.org/wiki/Depth-first_search
#
###############################################################################

from typing import Union


class SouthernGermany():
    Frankfurt = 'Frankfurt'
    Mannheim = 'Mannheim'
    Wurzburg = 'Wurzburg'
    Stuttgart = 'Stuttgart'
    Kassel = 'Kassel'
    Karlsruhe = 'Karsruhe'
    Erfurt = 'Erfurt'
    Nurnberg = 'Nurnberg'
    Augsburg = 'Augsburg'
    Munchen = 'Munchen'
    graph = {
        Frankfurt: [Mannheim, Wurzburg, Kassel],
        Mannheim: [Frankfurt, Karlsruhe],
        Wurzburg: [Frankfurt, Erfurt, Nurnberg],
        Stuttgart: [Nurnberg],
        Kassel: [Frankfurt, Munchen],
        Karlsruhe: [Mannheim, Augsburg],
        Erfurt: [Wurzburg],
        Nurnberg: [Wurzburg, Stuttgart, Munchen],
        Augsburg: [Karlsruhe, Munchen],
        Munchen: [Augsburg, Nurnberg, Kassel]
    }


class DFS():
    @staticmethod
    def iterative(Graph: dict, start: str, goal: str) -> Union[str, None]:

        # Add to stack (and mark as discovered).
        stack = [start]
        inspected = set()

        while stack:

            u = stack.pop()

            # Inspect node.
            if u not in inspected:

                if u == goal:
                    return u

                inspected.add(u)

                # Add adjacent nodes.
                for v in Graph[u]:

                    stack.append(v)

                print(u, stack, inspected)

        return None

    @staticmethod
    def recursive(Graph: dict, start: str, goal: str) -> Union[str, None]:

        discovered = {start}
        return DFS._recursive(Graph, start, goal, discovered)

    @staticmethod
    def _recursive(Graph: dict, start: str, goal: str,
                   discovered: set) -> Union[str, None]:
        """
        Recursive depth-first search.

        """
        # Inspect node.
        if start == goal:
            return start

        print(start, discovered)

        # Recursively, search adjacent nodes.
        # If not already inspected (avoid cycles).
        for v in Graph[start]:
            if v not in discovered:

                discovered.add(v)
                res = DFS._recursive(Graph, v, goal, discovered)
                if res is not None:
                    return res

        # Goal not found.
        return None


def main():

    graph = SouthernGermany.graph
    start = 'Frankfurt'
    goal = 'Berlin'

    res = DFS.iterative(graph, start, goal)
    print()
    print(res)


if __name__ == '__main__':
    main()


def test_found():

    graph = SouthernGermany.graph
    start = 'Frankfurt'
    goal = 'Erfurt'

    expect = 'Erfurt'

    actual = DFS.recursive(graph, start, goal)

    assert actual == expect


def test_not_found():

    graph = SouthernGermany.graph
    start = 'Frankfurt'
    goal = 'Berlin'

    expect = None

    actual = DFS.recursive(graph, start, goal)

    assert actual == expect
