class Trie():
    def __init__(self):
        """
        root (Node): Root of tree.
        currNode (Node): Reference to some node in tree.

        """
        self.root = self.Node()
        self.currNode = self.root

    class Node():
        def __init__(self):
            """
            children (dict): Maps char -> node.
            isWord (bool): Whether node marks end of a word.

            """
            self.children = {}
            self.isWord = False

    def startsWith(self, word):
        node = self._getWordNode(word)
        if node:
            return True
        else:
            return False

    def search(self, word):
        node = self._getWordNode(word)
        if node:
            return node.isWord
        else:
            return False

    def insert(self, word):
        node = self.root
        for char in word:
            if char not in node.children:
                node.children[char] = self.Node()
            node = node.children[char]
        node.isWord = True

    def _getWordNode(self, word):
        if not word:
            return None
        node = self.root
        for char in word:
            if char in node.children:
                node = node.children[char]
            else:
                return None
        return node


def test_search_empty_tree_empty_input():

    word = ''
    expect = False

    trie = Trie()
    actual = trie.search(word)

    assert actual == expect


def test_search_empty_tree():

    word = 'car'
    expect = False

    trie = Trie()
    actual = trie.search(word)

    assert actual == expect


def test_search_successfull():

    word = 'car'
    expect = True

    trie = Trie()
    trie.insert(word)
    actual = trie.search(word)

    assert actual == expect


def test_search_not_a_word():

    word = 'car'
    expect = False

    trie = Trie()
    trie.insert('caretaker')
    actual = trie.search(word)

    assert actual == expect


def test_startsWith_same_word():

    word = 'car'
    expect = True

    trie = Trie()
    trie.insert(word)
    actual = trie.startsWith(word)

    assert actual == expect


def test_startsWith_fail():

    word = 'car'
    expect = False

    trie = Trie()
    trie.insert('tomato')
    actual = trie.startsWith(word)

    assert actual == expect


def test_startsWith_true():

    word = 'car'
    expect = True

    trie = Trie()
    trie.insert('caretaker')
    actual = trie.startsWith(word)

    assert actual == expect
