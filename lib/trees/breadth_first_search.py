###############################################################################
# File name: breadth_first_search.py
# https://en.wikipedia.org/wiki/Breadth-first_search
#
###############################################################################

from typing import Union


class SouthernGermany():
    Frankfurt = 'Frankfurt'
    Mannheim = 'Mannheim'
    Wurzburg = 'Wurzburg'
    Stuttgart = 'Stuttgart'
    Kassel = 'Kassel'
    Karlsruhe = 'Karsruhe'
    Erfurt = 'Erfurt'
    Nurnberg = 'Nurnberg'
    Augsburg = 'Augsburg'
    Munchen = 'Munchen'
    graph = {
        Frankfurt: [Mannheim, Wurzburg, Kassel],
        Mannheim: [Frankfurt, Karlsruhe],
        Wurzburg: [Frankfurt, Erfurt, Nurnberg],
        Stuttgart: [Nurnberg],
        Kassel: [Frankfurt, Munchen],
        Karlsruhe: [Mannheim, Augsburg],
        Erfurt: [Wurzburg],
        Nurnberg: [Wurzburg, Stuttgart, Munchen],
        Augsburg: [Karlsruhe, Munchen],
        Munchen: [Augsburg, Nurnberg, Kassel]
    }


def bfs(Graph: dict, start: str, goal: str) -> Union[str, None]:
    """
    Breadth-first search.

    Notes:
      - Nodes must be marked as discovered upon adding to the queue,
        lest they get added multiple times if the graph has cycles.

    """

    # Add to queue (and mark as discovered).
    queue = [start]
    discovered = {start}

    while queue:

        # Inspect node.
        u = queue.pop(0)
        if u == goal:
            return u

        print(u, queue, discovered)

        # Add nodes at next level.
        for v in Graph[u]:
            if v not in discovered:

                # Add to queue (and mark as discovered).
                queue.append(v)
                discovered.add(v)

    # Goal not found.
    return None


def main():

    graph = SouthernGermany.graph
    start = 'Frankfurt'
    goal = 'Berlin'

    res = bfs(graph, start, goal)
    print()
    print(res)


if __name__ == '__main__':
    main()


def test_found():

    graph = SouthernGermany.graph
    start = 'Frankfurt'
    goal = 'Erfurt'

    expect = 'Erfurt'

    actual = bfs(graph, start, goal)

    assert actual == expect


def test_not_found():

    graph = SouthernGermany.graph
    start = 'Frankfurt'
    goal = 'Berlin'

    expect = None

    actual = bfs(graph, start, goal)

    assert actual == expect
