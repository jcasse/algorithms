from tree_node import TreeNode


class BST():
    def __init__(self):
        """
        Initialize binary search tree to the empty tree.

        """
        self.root = None
